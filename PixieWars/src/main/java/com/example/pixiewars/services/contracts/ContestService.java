package com.example.pixiewars.services.contracts;

import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ContestService {
    List<Contest> getAll();

    List<Contest> filter(FilterOptionsContests filterOptionsContests);

    Contest get(int id);

    Contest create(Contest contest, User authenticatedUser, MultipartFile file);

    Contest update(Contest contest, User authenticatedUser, MultipartFile file);

    void delete(int id, User authenticatedUser);
}
