package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.models.*;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import com.example.pixiewars.repositories.contracts.ReviewRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import com.example.pixiewars.services.contracts.InvitationService;
import com.example.pixiewars.services.contracts.ParticipationService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class ParticipationServiceImpl implements ParticipationService {
    public static final String YOU_ARE_JURY_EXCEPTION = "You can't participate in this contest, because you are jury!";

    public static final String YOU_ARENT_JURY_EXCEPTION = "You can't review this participation, because you arent jury!";
    public static final String YOU_ALREADY_REVIEWED_EXCEPTION = "You already reviewed this participation!";
    public static final int ONE_POINT_FOR_PARTICIPATION = 1;
    public static final String MISSING_PHOTO_EXCEPTION = "You need to add photo.";
    public static final String YOU_ARE_NOT_INVITED_EXCEPTION = "You are not invited to this contest!";
    public static final String PHASE_ONE_FINISHED_ERROR = "Phase one is already finished. You can't participate anymore!";
    public static final String PHASE_TWO_FINISHED_ERROR = "Phase two is already finished. You can't review anymore!";
    public static final int MINUS_DEFAULT_SCORE = 3;
    public static final int THREE_POINTS_FOR_JURY = 3;
    public static final int THREE_POINT_FOR_PARTICIPATION = 3;
    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";
    private final ParticipationRepository repository;
    private final ContestRepository contestRepository;
    private final UserRepository userRepository;

    private final ReviewRepository reviewRepository;
    private final FileUploadUtil fileUploadUtil;

    private final InvitationService invitationService;


    public ParticipationServiceImpl(ParticipationRepository repository, ContestRepository contestRepository, UserRepository userRepository, ReviewRepository reviewRepository, FileUploadUtil fileUploadUtil, InvitationService invitationService) {
        this.repository = repository;
        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.reviewRepository = reviewRepository;
        this.fileUploadUtil = fileUploadUtil;
        this.invitationService = invitationService;
    }


    public List<Participation> getAll(Integer limit){
        return repository.getAll(limit);
    }

    @Override
    public List<Participation> getAllByContest(int contestId, FilterOptions filterOptions) {
        contestRepository.get(contestId);
        return repository.getAllByContest(contestId, filterOptions);
    }

    @Override
    public List<Participation> getAllByUser(int userId) {
        userRepository.get(userId);
        return repository.getAllByUser(userId);
    }

    @Override
    public Participation get(int id, int participationId) {
        contestRepository.get(id);
        return repository.get(participationId);
    }

    @Override
    public Participation create(int id, Participation participation, User user, MultipartFile file) {
        Contest contest = contestRepository.get(id);
        int jurySize = contest.getJuryMembers().size();
        Date currentTime = new Date();
        if (currentTime.after(contest.getPhase_one_time_limit())) {
            throw new AuthorizationException(PHASE_ONE_FINISHED_ERROR);
        }
        Set<User> jury = contest.getJuryMembers();
        if (jury.contains(user)) {
            throw new AuthorizationException(YOU_ARE_JURY_EXCEPTION);
        }
        participation.setContest(contest);
        participation.setCreator(user);

        //img
        if (!file.isEmpty()) {
            participation.setPhoto(fileUploadUtil.addPhoto(file));
        } else {
            throw new AuthorizationException(MISSING_PHOTO_EXCEPTION);
        }

        //points, invited
        if (contest.isPrivate()) {
            List<Invitation> invitations = invitationService.getAllByContest
                    (contest.getContestId(), new FilterOptions(false));
            List<Invitation> forParticipant = invitations.stream().filter(i -> i.getUserId() == user.getId()).toList();
            if (forParticipant.size() > 0) {
                user.setPoints(user.getPoints() + THREE_POINT_FOR_PARTICIPATION);
                userRepository.update(user);
            } else {
                throw new AuthorizationException(YOU_ARE_NOT_INVITED_EXCEPTION);
            }
            invitationService.delete(forParticipant.get(0).getInvitationId());
        } else {
            user.setPoints(user.getPoints() + ONE_POINT_FOR_PARTICIPATION);
            userRepository.update(user);
        }
        participation.setTotalScore(participation.getTotalScore() + (jurySize * THREE_POINTS_FOR_JURY));

        return repository.create(participation);
    }

    @Override
    public void delete(int contestId, int participationId, User user) {
        contestRepository.get(contestId);
        Participation participation = repository.get(participationId);
        if(!user.isOrganiser()){
            if(!user.equals(participation.getCreator())){
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }
        }
        repository.delete(participationId);
    }

    @Override
    public List<Review> getReviews(int contestId, int participationId) {
        contestRepository.get(contestId);
        repository.get(participationId);
        return reviewRepository.getAll(participationId);
    }

    @Override
    public Review getReview(int contestId, int participationId, int reviewId) {
        contestRepository.get(contestId);
        repository.get(participationId);
        return reviewRepository.get(reviewId);
    }

    @Override
    public Review addReview(User user, Review review, int contestId, int participationId) {
        List<Review> allReviews = reviewRepository.getAll(participationId);
        Participation currentParticipation = repository.get(participationId);
        Contest currentContest = contestRepository.get(contestId);

        Date currentTime = new Date();
        if (currentTime.after(currentContest.getPhase_two_time_limit())) {
            throw new AuthorizationException(PHASE_TWO_FINISHED_ERROR);
        }

        Set<User> jury = currentContest.getJuryMembers();
        if (!jury.contains(user)) {
            throw new AuthorizationException(YOU_ARENT_JURY_EXCEPTION);
        }
        List<Review> userReview = allReviews.stream().filter(review1 -> review1.getJuryMember().equals(user)).toList();
        if (userReview.size() > 0) {
            throw new AuthorizationException(YOU_ALREADY_REVIEWED_EXCEPTION);
        }
        review.setJuryMember(user);
        review.setParticipationId(participationId);

        currentParticipation.setTotalScore(currentParticipation.getTotalScore() + review.getScore() - MINUS_DEFAULT_SCORE);
        repository.update(currentParticipation);

        return reviewRepository.create(review);
    }

    @Override
    public void deleteReview(User user, int reviewId, int contestId, int participationId) {
        checkJuryAndCreatorReviewPermissions(reviewId, contestId, user);
        contestRepository.get(contestId);
        Review reviewToDelete = reviewRepository.get(reviewId);
        Participation currentParticipation = repository.get(participationId);
        currentParticipation.setTotalScore(currentParticipation.getTotalScore() - reviewToDelete.getScore());
        repository.update(currentParticipation);
        reviewRepository.delete(reviewId);
    }

    private void checkIsAdmin(User authenticatedUser) {
        if (!authenticatedUser.isOrganiser()) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

    private void checkJuryAndCreatorReviewPermissions(int reviewId, int contestId, User user) {
        Review review = reviewRepository.get(reviewId);
        Set<User> jury = contestRepository.get(contestId).getJuryMembers();
        if (!(jury.contains(user) || review.getJuryMember().equals(user))) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }
}
