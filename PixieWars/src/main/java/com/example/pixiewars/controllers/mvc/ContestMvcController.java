package com.example.pixiewars.controllers.mvc;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.ContestMapper;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.helpers.ParticipationMapper;
import com.example.pixiewars.models.*;
import com.example.pixiewars.models.dtos.*;
import com.example.pixiewars.services.contracts.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    private final UserService userService;
    private final ContestService contestService;
    private final ContestMapper contestMapper;
    private final CategoryService categoryService;
    private final InvitationService invitationService;

    private final ParticipationService participationService;
    private final ParticipationMapper participationMapper;


    private final AuthenticationHelper authenticationHelper;
    private final FileUploadUtil fileUploadUtil;


    public ContestMvcController(UserService userService, ParticipationService participationService, InvitationService invitationService, ContestService contestService, CategoryService categoryService, ContestMapper contestMapper, ParticipationMapper participationMapper, AuthenticationHelper authenticationHelper, FileUploadUtil fileUploadUtil) {
        this.userService = userService;
        this.participationService = participationService;
        this.invitationService = invitationService;
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.contestMapper = contestMapper;
        this.participationMapper = participationMapper;
        this.authenticationHelper = authenticationHelper;
        this.fileUploadUtil = fileUploadUtil;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganiser")
    public boolean populateIsOrganiser(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            String userEmail = (String) session.getAttribute("currentUser");
            User user = userService.getByEmail(userEmail);
            return user.isOrganiser();
        }
        return false;
    }

    @ModelAttribute("authUser")
    public User currentUSer(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            String userEmail = (String) session.getAttribute("currentUser");
            return userService.getByEmail(userEmail);
        }
        return null;
    }

    @ModelAttribute("categories")
    public List<Category> populateStyles() {
        return categoryService.get();
    }

    @GetMapping
    public String showAllContests(@ModelAttribute("filterContests") FilterContestsDto dto, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        FilterOptionsContests fo = new FilterOptionsContests(dto.getContestTitle(), dto.getCategoryId(), dto.getPrivate(),
                null, null, dto.getSort());

        model.addAttribute("filterContests", dto);
        model.addAttribute("contests", contestService.filter(fo));
        model.addAttribute("contestsPhaseOne", contestService.filter(new FilterOptionsContests(null, null, null,
                null, "phase_one", null)));
        model.addAttribute("contestsPhaseTwo", contestService.filter(new FilterOptionsContests(null, null, null,
                null, "phase_two", null)));
        model.addAttribute("contestsFinished", contestService.filter(new FilterOptionsContests(null, null, null,
                true, null, null)));
        return "ContestsView";
    }


    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.get(id);
            Date currentTime = new Date();
            Date phaseOneTimeLimit = new Date(contest.getPhase_one_time_limit().getTime());

            boolean isPhaseOne = currentTime.before(phaseOneTimeLimit);
            boolean isPhaseTwo = currentTime.after(phaseOneTimeLimit);
            boolean isUserJuryMember = contest.getJuryMembers().contains(user);


            boolean isUserInvited = false;
            List<Invitation> invited = invitationService.getAllByContest(id,new FilterOptions(null,false,null));
            for (Invitation invitation : invited) {
                if(invitation.getUserId() == user.getId()){
                    isUserInvited = true;
                    break;
                }
            }


            boolean isAlreadyJoined = false;
            List<Participation> participations = participationService.getAllByContest(contest.getContestId(), new FilterOptions());
            if(contest.isChecked()){
                participations = participationService.getAllByContest(contest.getContestId(), new FilterOptions(null,null,"totalScore desc"));
            }

            for (Participation participation : participations) {
                if (participation.getCreator().equals(user)) {
                    isAlreadyJoined = true;
                    break;
                }
            }

            model.addAttribute("contest", contest);
            model.addAttribute("contestId", contest.getContestId());
            model.addAttribute("isPhaseOne", isPhaseOne);
            model.addAttribute("isPhaseTwo", isPhaseTwo);
            model.addAttribute("isFinished", contest.isChecked());
            model.addAttribute("participations", participations);
            model.addAttribute("isUserJuryMember", isUserJuryMember);
            model.addAttribute("isAlreadyJoined", isAlreadyJoined);
            model.addAttribute("isUserInvited", isUserInvited);


            return "ContestView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/photo")
    public String showContestPhoto(@PathVariable int id, Model model, HttpSession session) {

        try {
             authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.get(id);
            model.addAttribute("photo", contest.getCoverPhoto());
            return "PhotoFullSize";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    @GetMapping("/new")
    public String showNewContestPage(Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contest", new ContestDto());
        List<User> juryChoices = getUsersForJury(authenticatedUser);
        model.addAttribute("juryChoices", juryChoices);
        return "ContestCreateView";
    }


    private List<User> getUsersForJury(User authenticatedUser) {
        List<User> allMasterUsers = userService.filter(authenticatedUser, new FilterOptionsUsers(null, null, null, null, "Master", "username asc"));
        List<User> allDictatorUsers = userService.filter(authenticatedUser, new FilterOptionsUsers(null, null, null, null, "Dictator", null));
        return Stream.concat(allDictatorUsers.stream(), allMasterUsers.stream()).collect(Collectors.toList());
    }


    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contest") ContestDto contestDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session, @RequestParam("file") MultipartFile file) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            List<User> juryChoices = getUsersForJury(user);
            model.addAttribute("juryChoices", juryChoices);
            return "ContestCreateView";
        }
        try {
            Contest contest = contestMapper.fromDto(contestDto);
            if (file.isEmpty()) {
                contest.setCoverPhoto(null);
            }
            Contest createdContest = contestService.create(contest, user, file);
            for (Integer potentialJuror : contestDto.getJuryInvitations()) {
                if (potentialJuror != null) {
                    invitationService.inviteJury(potentialJuror, createdContest.getContestId());
                }
            }

            return "redirect:/contests/" + createdContest.getContestId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (MaxUploadSizeExceededException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteContests(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            contestService.delete(id, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    //////////////////////////////////////////////////////////////////
    @GetMapping("/{id}/invite-participants")
    public String showNewInviteParticipantsPage(@PathVariable int id, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        List<User> inviteParticipantsUsers = userService.filter(authenticatedUser, new FilterOptionsUsers(null,
                null, null, false, null, "username asc"));

        Contest currentContest = contestService.get(id);
        ContestDto currentContestDto = contestMapper.toDto(currentContest);

        List<User> contestJury = currentContest.getJuryMembers().stream().toList();
        for (User jury : contestJury) {
            inviteParticipantsUsers.remove(jury);
        }

        List<Invitation> allInvitationsForContest =
                invitationService.getAllByContest(id, new FilterOptions(null, null, null));
        for (Invitation invitation : allInvitationsForContest) {
            inviteParticipantsUsers.remove(userService.get(invitation.getUserId()));
        }

        model.addAttribute("inviteParticipants", inviteParticipantsUsers);
        model.addAttribute("contestDto", currentContestDto);
        model.addAttribute("contestId", id);


        return "InviteParticipantsView";
    }


    @PostMapping("/{id}/invite-participants")
    public String InviteParticipantsPage(@PathVariable int id,
                                         @Valid @ModelAttribute("contestDto") ContestDto contestDto,
                                         BindingResult bindingResult,
                                         Model model,
                                         HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "InviteParticipantsView";
        }
        try {
            for (Integer potentialParticipant : contestDto.getParticipantInvitations()) {
                if (potentialParticipant != null) {
                    invitationService.inviteParticipation(potentialParticipant, id);
                }
            }

            return "redirect:/contests/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    //////////////////////////////////////////////////////////////////
    //CATEGORY
    @GetMapping("/categories/new")
    public String showNewCategoryPage(Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("category", new CategoryDto());

        return "CategoryCreateView";
    }


    @PostMapping("/categories/new")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDto categoryDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "CategoryCreateView";
        }
        try {
            Category category = new Category();
            category.setCategory_name(categoryDto.getCategoryName());
            categoryService.create(user, category);

            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException | EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    //////////////////////////////////////////////////////////////////
    //PARTICIPATIONS

    @GetMapping("/{contestId}/participations/{participationId}") //http://localhost:8080/contests/5/participations/4
    public String showSingleParticipation(@PathVariable int contestId,
                                          @PathVariable int participationId,
                                          Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.get(contestId);
            Date currentTime = new Date();
            Date phaseOneTimeLimit = new Date(contest.getPhase_one_time_limit().getTime());
            boolean isPhaseTwo = currentTime.after(phaseOneTimeLimit);
            boolean isUserJuryMember = contest.getJuryMembers().contains(user);

            Participation participation = participationService.get(contestId, participationId);
            List<Review> allReviews = participationService.getReviews(contestId, participationId);
            boolean hasReviewed = false;
            for (Review review : allReviews) {
                if (review.getJuryMember().equals(user)) {
                    hasReviewed = true;
                    break;
                }
            }
            boolean isCreator = participation.getCreator().equals(user);
            model.addAttribute("isCreator", isCreator);

            model.addAttribute("participation", participation);
            model.addAttribute("participationId", participation.getId());
            model.addAttribute("contest", contest);
            model.addAttribute("contestId", contest.getContestId());
            model.addAttribute("isPhaseTwo", isPhaseTwo);
            model.addAttribute("isFinished", contest.isChecked());
            model.addAttribute("isUserJuryMember", isUserJuryMember);
            model.addAttribute("hasReviewed", hasReviewed);
            model.addAttribute("allReviews", allReviews);
            model.addAttribute("newReview", new ReviewDto());

            return "ParticipationView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{contestId}/participations/new")
    public String showNewParticipationPage(@PathVariable int contestId, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("participation", new ParticipationDto());

        return "ParticipationCreateView";
    }

    @PostMapping("/{contestId}/participations/new")
    public String createParticipation(@PathVariable int contestId,
                                      @Valid @ModelAttribute("participation") ParticipationDto participationDto,
                                      BindingResult bindingResult,
                                      Model model,
                                      HttpSession session,
                                      @RequestParam("file") MultipartFile file) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "ParticipationCreateView";
        }

        try {
            Participation participation = participationMapper.fromDto(participationDto);
            Participation createdParticipation = participationService.create(contestId, participation, user, file);
            return "redirect:/contests/" + contestId + "/participations/" + createdParticipation.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }
    @GetMapping("/{id}/participations/{participationId}/delete")
    public String deleteParticipation(@PathVariable int id,@PathVariable int participationId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            participationService.delete(id,participationId,user);
            return "redirect:/contests/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/participations/{participationId}/photo")
    public String showParticipationPhoto(@PathVariable int id, @PathVariable int participationId, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            contestService.get(id);
            Participation participation = participationService.get(id, participationId);
            model.addAttribute("photo", participation.getPhoto());
            return "PhotoFullSize";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    //////////////////////////////////////////////////////////////////
    //REVIEWS


    @PostMapping("/{contestId}/participations/{participationId}/review/new")
    public String createReview(@Valid @ModelAttribute("newReview") ReviewDto reviewDto,
                               @PathVariable int contestId,
                               @PathVariable int participationId,
                               BindingResult bindingResult,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "ParticipationView";
        }
        try {
            Review review = participationMapper.fromDtoReview(reviewDto);
            participationService.addReview(user, review, contestId, participationId);

            return "redirect:/contests/" + contestId + "/participations/" + participationId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

}
