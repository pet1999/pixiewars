create table categories
(
    category_id   int auto_increment
        primary key,
    category_name varchar(20) not null,
    constraint categories_pk2
        unique (category_id)
);

create table contests
(
    contest_id           int auto_increment
        primary key,
    title                varchar(64)       not null,
    description          varchar(200)      not null,
    category_id          int               not null,
    is_private           tinyint default 0 not null,
    phase_one_time_limit datetime          not null,
    phase_two_time_limit datetime          not null,
    cover_photo          varchar(1000)     not null,
    prize                int               null,
    isChecked            tinyint default 0 not null,
    constraint contests_pk2
        unique (contest_id),
    constraint contests_pk3
        unique (title),
    constraint contests_categories_fk
        foreign key (category_id) references categories (category_id)
            on update cascade on delete cascade
);

create table users
(
    user_id              int auto_increment
        primary key,
    username             varchar(20)       not null,
    first_name           varchar(20)       not null,
    last_name            varchar(20)       not null,
    password             varchar(200)      not null,
    email                varchar(50)       not null,
    avatar_photo         varchar(1000)     null,
    is_organizer         tinyint default 0 not null,
    points               int     default 0 not null,
    `rank`               varchar(20)       not null,
    is_deleted           tinyint default 0 not null,
    reset_password_token varchar(30)       null,
    token_expiry_date    datetime          null,
    contest_wins         int     default 0 not null,
    constraint users_pk
        unique (email),
    constraint users_pk2
        unique (user_id),
    constraint users_pk3
        unique (username)
);

create table invitations
(
    invitation_id int auto_increment
        primary key,
    user_id       int               not null,
    contest_id    int               not null,
    message       varchar(250)      not null,
    for_jury      tinyint default 0 not null,
    answer        tinyint           null,
    constraint invitations_pk2
        unique (invitation_id),
    constraint invitations_contests_fk
        foreign key (contest_id) references contests (contest_id)
            on update cascade on delete cascade,
    constraint invitations_users_fk
        foreign key (user_id) references users (user_id)
            on update cascade on delete cascade
);

create table jury_members
(
    jury_member_id int auto_increment
        primary key,
    contest_id     int not null,
    user_id        int not null,
    constraint jury_members_pk2
        unique (jury_member_id),
    constraint jury_members_contests_fk
        foreign key (contest_id) references contests (contest_id)
            on update cascade on delete cascade,
    constraint jury_members_users_fk
        foreign key (user_id) references users (user_id)
);

create table participations
(
    participation_id int auto_increment
        primary key,
    user_id          int           not null,
    contest_id       int           not null,
    title            varchar(64)   not null,
    story            varchar(200)  not null,
    photo            varchar(1000) not null,
    total_score      int default 0 not null,
    position         int           null,
    constraint participations_pk2
        unique (participation_id),
    constraint participations_contests_fk
        foreign key (contest_id) references contests (contest_id)
            on update cascade on delete cascade,
    constraint participations_users_fk
        foreign key (user_id) references users (user_id)
            on update cascade on delete cascade
);

create table reviews
(
    review_id        int auto_increment
        primary key,
    user_id          int           not null,
    participation_id int           not null,
    score            int default 0 not null,
    comment          varchar(200)  not null,
    constraint reviews_pk2
        unique (review_id),
    constraint reviews_participations_fk
        foreign key (participation_id) references participations (participation_id)
            on update cascade on delete cascade,
    constraint reviews_users_fk
        foreign key (user_id) references users (user_id)
            on update cascade on delete cascade
);

