package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.pixiewars.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    ContestRepository mockContestRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(null);

        // Act
        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    void get_Should_CallRepository_withFilter() {
        // Arrange
        User authenticatedUser = createMockUser();
        FilterOptionsUsers filterOptionsUsers = new FilterOptionsUsers();
        Mockito.when(mockRepository.filter(filterOptionsUsers)).thenReturn(null);

        // Act
        service.filter(authenticatedUser, filterOptionsUsers);

        Mockito.verify(mockRepository, Mockito.times(1)).filter(filterOptionsUsers);
    }

    @Test
    public void get_Should_ReturnUser_When_MatchByIdExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        User result = service.get(mockUser.getId());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void get_Should_ReturnUser_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        User result = service.get(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void get_Should_ReturnUser_When_MatchByEmailExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByEmail(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        User result = service.getByEmail(mockUser.getEmail());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void create_Should_CallRepository_When_UserWithSameUserNameAndEmailDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void create_Should_Throw_When_UserWithSameUserNameExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> service.create(mockUser));
    }

    @Test
    public void create_Should_Throw_When_UserWithSameEmailExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> service.create(mockUser));
    }

    @Test
    void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.update(mockUser, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void update_Should_Throw_When_UserIsNotCreator() {
        // Arrange
        User authenticatedUser = createMockUser();
        User mockUser = createMockUserTwo();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.update(mockUser, mockUser);

        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(authenticatedUser, mockUser));
    }

    @Test
    void update_Should_Throw_When_UserNameIsDifferent() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        User mockUser = createMockUserTwo();
        User mockUser1 = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser1);

        // Act
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(authenticatedUser, mockUser));
    }

    @Test
    void update_Should_Throw_When_EmailIsDifferent() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        User mockUser = createMockUserTwo();
        User mockUser1 = createMockUser();
        mockUser1.setUsername(mockUser.getUsername());

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser1);

        // Act
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(authenticatedUser, mockUser));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.delete(1, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void makeOrganiser_Should_CallRepository_When_UserIsNotOrganiser() {
        // Arrange
        User mockUser = createMockUser();
        User mockAuthenticatedUser = createMockOrganiser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.makeOrganiser(mockAuthenticatedUser, mockUser.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void demoteOrganiser_Should_CallRepository_When_UserIsOrganiser() {
        // Arrange
        User mockUser = createMockOrganiserTwo();
        User mockAuthenticatedUser = createMockOrganiser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.demoteOrganiser(mockAuthenticatedUser, mockUser.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void demoteOrganiser_Should_Throw_When_DemotingItself() {
        // Arrange
        User mockUser = createMockOrganiser();
        User mockAuthenticatedUser = createMockOrganiser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.demoteOrganiser(mockAuthenticatedUser, mockUser.getId()));
    }

    @Test
    void demoteOrganiser_Should_Throw_When_AuthenticatedUserIsNotOrganiser() {
        // Arrange
        User mockUser = createMockOrganiser();
        User mockAuthenticatedUser = createMockUser();


        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.demoteOrganiser(mockAuthenticatedUser, mockUser.getId()));
    }

    @Test
    void getAllContests_Should_CallRepository_When_UserIsJury() {
        // Arrange
        User mockAuthenticatedUser = createMockUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockAuthenticatedUser);
        Mockito.when(mockContestRepository.filter(Mockito.any()))
                .thenReturn(createMockContestList());
        // Act
        service.getAllContestsWhereUserIsJury(mockAuthenticatedUser, mockAuthenticatedUser.getId());

        // Assert
        Mockito.verify(mockContestRepository).filter(Mockito.any());
    }

    @Test
    void getAllContests_Should_Throw_When_AuthenticatedUserAndUserAreNotTheSame() {
        // Arrange
        User mockAuthenticatedUser = createMockUser();
        User mockUser = createMockUserTwo();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.getAllContestsWhereUserIsJury(mockAuthenticatedUser, mockUser.getId()));
    }

    @Test
    void getByReset_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRepository.getByToken(Mockito.anyString())).thenReturn(null);

        // Act
        service.getByResetPasswordToken("token");

        Mockito.verify(mockRepository, Mockito.times(1)).getByToken("token");
    }

    @Test
    void updateResetPasswordToken_Should_CallRepository() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenReturn(mockUser);

        // Act
        service.updateResetPasswordToken("token","email");

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void updatePassword_Should_CallRepository() {
        // Arrange
        User mockUser = createMockUser();

        // Act
        service.updatePassword(mockUser,"newPassword");

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockUser);
    }
}
