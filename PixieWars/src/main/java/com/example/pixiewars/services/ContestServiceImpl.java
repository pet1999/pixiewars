package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import com.example.pixiewars.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static java.lang.String.format;

@Service
public class ContestServiceImpl implements ContestService {
    public static final String MISSING_PHOTO_EXCEPTION = "You need to add photo.";
    public static final String YOU_CANNOT_CHANGE_VARIABLE = "You cannot change %s.";
    public static final String PHASE_TWO_TIME_LIMIT = "phase two time limit";
    public static final String PHASE_ONE_TIME_LIMIT = "phase one time limit";
    public static final String TITLE = "title";
    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";
    private final ContestRepository contestRepository;

    private final UserRepository userRepository;

    private final FileUploadUtil fileUploadUtil;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, UserRepository userRepository, FileUploadUtil fileUploadUtil) {
        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.fileUploadUtil = fileUploadUtil;
    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public List<Contest> filter(FilterOptionsContests filterOptionsContests) {
        return contestRepository.filter(filterOptionsContests);
    }


    @Override
    public Contest get(int id) {
        return contestRepository.get(id);
    }


    @Override
    public Contest create(Contest contest, User authenticatedUser, MultipartFile file) {
        checkIsAdmin(authenticatedUser);
        boolean duplicateTitleExists = true;
        try {
            contestRepository.get(contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateTitleExists = false;
        }
        if (duplicateTitleExists) {
            throw new EntityDuplicateException("Contest", "title", contest.getTitle());
        }
        List<User> allOrganizers = userRepository.filter(new FilterOptionsUsers(null, null, null,
                true, null, null));
        contest.getJuryMembers().addAll(allOrganizers);

        if (contest.getCoverPhoto() == null) {
            if (!file.isEmpty()) {
                contest.setCoverPhoto(fileUploadUtil.addPhoto(file));
            } else {
                throw new AuthorizationException(MISSING_PHOTO_EXCEPTION);
            }
        }

        return contestRepository.create(contest);
    }


    @Override
    public Contest update(Contest contest, User authenticatedUser, MultipartFile file) {
        checkIsAdmin(authenticatedUser);
        Contest existingContest = contestRepository.get(contest.getContestId());
        if (!existingContest.getTitle().equalsIgnoreCase(contest.getTitle())) {
            throw new AuthorizationException(format(YOU_CANNOT_CHANGE_VARIABLE, TITLE));
        }
        if (!existingContest.getPhase_one_time_limit().equals(contest.getPhase_one_time_limit())) {
            throw new AuthorizationException(format(YOU_CANNOT_CHANGE_VARIABLE, PHASE_ONE_TIME_LIMIT));
        }
        if (!existingContest.getPhase_two_time_limit().equals(contest.getPhase_two_time_limit())) {
            throw new AuthorizationException(format(YOU_CANNOT_CHANGE_VARIABLE, PHASE_TWO_TIME_LIMIT));
        }

        if (!file.isEmpty()) {
            contest.setCoverPhoto(fileUploadUtil.addPhoto(file));
        }

        return contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User authenticatedUser) {
        contestRepository.get(id);
        checkIsAdmin(authenticatedUser);
        contestRepository.delete(id);
    }

    private void checkIsAdmin(User authenticatedUser) {
        if (!authenticatedUser.isOrganiser()) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

}
