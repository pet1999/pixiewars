package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Participation;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ParticipationRepositoryImpl implements ParticipationRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ParticipationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Participation> getAll(Integer limit) {
        try (Session session = sessionFactory.openSession()) {
            Query<Participation> query = session.createQuery(
                    "SELECT p FROM Participation p ORDER BY p.totalScore DESC", Participation.class);
            if(limit != null){
                query.setMaxResults(limit);
            }
            return query.list();
        }

    }


    @Override
    public List<Participation> getAllByContest(int contestId, FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Participation ");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filter.add(" contest.id = :contestId ");
            queryParams.put("contestId", contestId);

            queryString.append(" where ").append(String.join(" and ", filter));

            if (filterOptions.getSort().isPresent()) {
                queryString.append(" order by ");

                String[] sortParams = filterOptions.getSort().get().split(" ");

                queryString.append(" totalScore ");

                if (sortParams.length > 1) {
                    if (sortParams[1].equalsIgnoreCase("desc")) {
                        queryString.append(" desc ");
                    } else {
                        throw new UnsupportedOperationException(
                                "The second parameter in the sort should be either 'asc' or 'desc'!");
                    }
                }
            }

            Query<Participation> queryList = session.createQuery(queryString.toString(), Participation.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);

            return queryList.list();
        }
    }

    @Override
    public List<Participation> getAllByUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Participation> query = session.createQuery("from Participation where creator.id = :userId", Participation.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Participation get(int participationId) {
        try (Session session = sessionFactory.openSession()) {
            Participation participation = session.get(Participation.class, participationId);
            if (participation == null) {
                throw new EntityNotFoundException("Participation", participationId);
            }
            return participation;
        }
    }

    @Override
    public Participation get(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Participation> query = session.createQuery("from Participation where title = :title", Participation.class);
            query.setParameter("title", title);

            List<Participation> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Participation", "title", title);
            }

            return result.get(0);
        }
    }

    @Override
    public Participation create(Participation participation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(participation);
            session.getTransaction().commit();
            return participation;
        }
    }

    @Override
    public Participation update(Participation participation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(participation);
            session.getTransaction().commit();
            return participation;
        }
    }

    @Override
    public void delete(int participationId) {
        Participation participationToDelete = get(participationId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(participationToDelete);
            session.getTransaction().commit();
        }
    }
}
