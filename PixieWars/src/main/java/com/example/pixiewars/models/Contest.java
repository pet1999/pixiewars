package com.example.pixiewars.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "is_private")
    private boolean isPrivate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "phase_one_time_limit")
    private Date phase_one_time_limit;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "phase_two_time_limit")
    private Date phase_two_time_limit;

    @Column(name = "cover_photo")
    private String coverPhoto;

    @Column(name = "prize")
    private int prize;

    @Column(name = "isChecked")
    private boolean isChecked;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "jury_members",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> juryMembers;


    @Transient
    @JsonIgnore
    private Set<User> juryInvitations;

    @Transient
    @JsonIgnore
    private Set<User> participantInvitations;

    public Contest() {
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Date getPhase_one_time_limit() {
        return phase_one_time_limit;
    }

    public void setPhase_one_time_limit(Date phase_one_time_limit) {
        this.phase_one_time_limit = phase_one_time_limit;
    }

    public Date getPhase_two_time_limit() {
        return phase_two_time_limit;
    }

    public void setPhase_two_time_limit(Date phase_two_time_limit) {
        this.phase_two_time_limit = phase_two_time_limit;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Set<User> getJuryMembers() {
        return juryMembers;
    }

    public void setJuryMembers(Set<User> juryMembers) {
        this.juryMembers = juryMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return contestId == contest.contestId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestId);
    }
}
