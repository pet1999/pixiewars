package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Invitation;
import com.example.pixiewars.repositories.contracts.InvitationRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InvitationRepositoryImpl implements InvitationRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public InvitationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Invitation> getAllByContest(int contestId, FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Invitation ");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filter.add(" contestId = :contestId ");
            queryParams.put("contestId", contestId);

            filterOptions.getForJury().ifPresent(v -> {
                filter.add(" forJury = :forJury ");
                queryParams.put("forJury", v);
            });

            queryString.append(" where ").append(String.join(" and ", filter));

            Query<Invitation> queryList = session.createQuery(queryString.toString(), Invitation.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);

            return queryList.list();
        }
    }

    @Override
    public List<Invitation> getAllByUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Invitation> query = session.createQuery("from Invitation where userId = :userId", Invitation.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Invitation get(int invitationId) {
        try (Session session = sessionFactory.openSession()) {
            Invitation invitation = session.get(Invitation.class, invitationId);
            if (invitation == null) {
                throw new EntityNotFoundException("Invitation", invitationId);
            }
            return invitation;
        }
    }

    @Override
    public Invitation create(Invitation invitation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(invitation);
            session.getTransaction().commit();
            return invitation;
        }
    }


    @Override
    public void delete(int invitationId) {
        Invitation invitationToDelete = get(invitationId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(invitationToDelete);
            session.getTransaction().commit();
        }
    }
}
