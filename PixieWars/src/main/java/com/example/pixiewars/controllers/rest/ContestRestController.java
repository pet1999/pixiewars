package com.example.pixiewars.controllers.rest;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.ContestMapper;
import com.example.pixiewars.models.*;
import com.example.pixiewars.models.dtos.ContestDto;
import com.example.pixiewars.services.contracts.CategoryService;
import com.example.pixiewars.services.contracts.ContestService;
import com.example.pixiewars.services.contracts.InvitationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/contests")
public class ContestRestController {
    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";
    private final ContestService contestService;

    private final CategoryService categoryService;
    private final ContestMapper contestMapper;
    private final InvitationService invitationService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ContestRestController(ContestService contestService, CategoryService categoryService, ContestMapper contestMapper, InvitationService invitationService, AuthenticationHelper authenticationHelper) {
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.contestMapper = contestMapper;
        this.invitationService = invitationService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Contest> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return contestService.getAll();
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Contest> filter(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) String contestTitle,
                                @RequestParam(required = false) Integer categoryId,
                                @RequestParam(required = false) Boolean isPrivate,
                                @RequestParam(required = false) Boolean isChecked,
                                @RequestParam(required = false) String phase,
                                @RequestParam(required = false) String sort) {
        try {
            authenticationHelper.tryGetUser(headers);
            FilterOptionsContests filterOptionsContests = new FilterOptionsContests(contestTitle, categoryId, isPrivate, isChecked, phase, sort);
            return contestService.filter(filterOptionsContests);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Contest get(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return contestService.get(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(consumes = "multipart/form-data")
    public Contest create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestPart("data") ContestDto contestDto,
                          @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.fromDto(contestDto);
            return contestService.create(contest, user, file);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestPart("data") ContestDto contestDto,
                          @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.fromDto(id, contestDto);
            return contestService.update(contest, authenticatedUser, file);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            contestService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/categories")
    public List<Category> getCategories(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return categoryService.get();
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/categories")
    public Category createCategory(@RequestHeader HttpHeaders headers, @Valid @RequestBody Category category) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return categoryService.create(user, category);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/categories/{id}")
    public Category getCategory(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return categoryService.get(id);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/jury")
    public Set<User> getJuryMembers(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return contestService.get(id).getJuryMembers();
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/invitations/{type}")
    public List<Invitation> getInvitationsByContest(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                                    @PathVariable String type) {
        try {
            authenticationHelper.tryGetUser(headers);
            boolean forJury;
            if (type.equals("participant")) {
                forJury = false;
            } else if (type.equals("jury")) {
                forJury = true;
            } else {
                throw new AuthorizationException("No such invitation type.");
            }
            return invitationService.getAllByContest(id, new FilterOptions(null, forJury,null));
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/users/{userId}/invite_jury")
    public Invitation inviteJury(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable int userId) {
        try {
            if (!authenticationHelper.tryGetUser(headers).isOrganiser()) {
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }
            return invitationService.inviteJury(userId, id);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/users/{userId}/invite_participation")
    public Invitation inviteParticipation(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable int userId) {
        try {
            if (!authenticationHelper.tryGetUser(headers).isOrganiser()) {
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }
            return invitationService.inviteParticipation(userId, id);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
