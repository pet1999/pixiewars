package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Category;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.CategoryRepository;
import com.example.pixiewars.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> get() {
        return categoryRepository.getAll();
    }

    @Override
    public Category get(int id) {
        return categoryRepository.get(id);
    }

    @Override
    public Category getByName(String categoryName) {
        return categoryRepository.getByName(categoryName);
    }

    @Override
    public Category create(User authenticatedUser, Category category) {
        checkIsAdmin(authenticatedUser);
        boolean duplicateCategoryExists = true;
        try {
            categoryRepository.getByName(category.getCategory_name());
        } catch (EntityNotFoundException e) {
            duplicateCategoryExists = false;
        }
        if (duplicateCategoryExists) {
            throw new EntityDuplicateException("Category", "name", category.getCategory_name());
        }
        return categoryRepository.create(category);
    }

    private void checkIsAdmin(User authenticatedUser) {
        if (!authenticatedUser.isOrganiser()) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }
}
