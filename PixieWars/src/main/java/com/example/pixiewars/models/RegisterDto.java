package com.example.pixiewars.models;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class RegisterDto extends LoginDto {

    @NotEmpty(message = "Password confirmation can't be empty")
    private String passwordConfirm;
    @Size(min = 5, max = 20, message = "Username should be between 5 and 20 symbols.")
    private String username;

    @Size(min = 3, max = 25, message = "First name should be between 3 and 25 symbols.")
    private String firstName;

    @Size(min = 3, max = 32, message = "Last name should be between 3 and 25 symbols.")
    private String lastName;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
