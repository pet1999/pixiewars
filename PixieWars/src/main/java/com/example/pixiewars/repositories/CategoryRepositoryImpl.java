package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Category;
import com.example.pixiewars.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
    private final SessionFactory sessionFactory;

    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category order by category_name", Category.class);
            return query.list();
        }
    }

    @Override
    public Category get(int categoryId) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, categoryId);
            if (category == null) {
                throw new EntityNotFoundException("Category", categoryId);
            }
            return category;
        }
    }

    @Override
    public Category getByName(String categoryName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where category_name = :categoryName", Category.class);
            query.setParameter("categoryName", categoryName);

            List<Category> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Category", "name", categoryName);
            }

            return result.get(0);
        }
    }

//    @Override
//    public Contest get(String title) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<Contest> query = session.createQuery("from Contest where title = :title", Contest.class);
//            query.setParameter("title", title);
//
//            List<Contest> result = query.list();
//            if (result.size() == 0) {
//                throw new EntityNotFoundException("Contest", "title", title);
//            }
//
//            return result.get(0);
//        }
//    }

    @Override
    public Category create(Category category) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(category);
            session.getTransaction().commit();
            return category;
        }
    }

    @Override
    public void delete(int categoryId) {
        Category categoryToDelete = get(categoryId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(categoryToDelete);
            session.getTransaction().commit();
        }
    }
}
