package com.example.pixiewars.models;

import java.util.Optional;

public class FilterOptionsUsers {

    private Optional<String> username;
    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<Boolean> isOrganiser;
    private Optional<String> rank;
    private Optional<String> sort;

    public FilterOptionsUsers() {
        this(null, null, null, null, null, null);
    }

    public FilterOptionsUsers(String username, String firstName, String lastName,
                              Boolean isOrganiser, String rank, String sort) {
        this.username = Optional.ofNullable(username);
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.isOrganiser = Optional.ofNullable(isOrganiser);
        this.rank = Optional.ofNullable(rank);
        this.sort = Optional.ofNullable(sort);
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public Optional<Boolean> getIsOrganiser() {
        return isOrganiser;
    }

    public Optional<String> getRank() {
        return rank;
    }

    public Optional<String> getSort() {
        return sort;
    }
}
