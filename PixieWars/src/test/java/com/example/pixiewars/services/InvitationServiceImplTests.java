package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Invitation;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.InvitationRepository;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.example.pixiewars.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class InvitationServiceImplTests {
    @Mock
    ContestRepository contestRepository;

    @Mock
    InvitationRepository invitationRepository;

    @Mock
    ParticipationRepository participationRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    InvitationServiceImpl service;

    @Test
    void getAllByContest_Should_CallRepository() {
        Contest contest = createMockContest();
        // Arrange
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(invitationRepository.getAllByContest(Mockito.anyInt(),
                Mockito.any(FilterOptions.class))).thenReturn(null);

        // Act
        int contestId = 1;
        FilterOptions filterOptions = new FilterOptions();
        service.getAllByContest(contestId, filterOptions);

        // Assert
        Mockito.verify(invitationRepository, Mockito.times(1))
                .getAllByContest(Mockito.anyInt(), Mockito.any(FilterOptions.class));
    }

    @Test
    void getAllByUser_Should_CallRepository() {
        User authenticatedUser = createMockUser();
        // Arrange
        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(authenticatedUser);
        Mockito.when(invitationRepository.getAllByUser(Mockito.anyInt()))
                .thenReturn(null);

        // Act
        service.getAllByUser(authenticatedUser, authenticatedUser.getId());

        // Assert
        Mockito.verify(invitationRepository, Mockito.times(1))
                .getAllByUser(Mockito.anyInt());
    }

    @Test
    void getAllByUser_Should_Throw_IfUserIsDifferent() {
        User authenticatedUser = createMockUser();
        User user = createMockUserTwo();
        // Arrange
        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(user);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.getAllByUser(authenticatedUser, authenticatedUser.getId()));
    }

    @Test
    public void get_Should_ReturnInvitation_When_MatchByIdExist() {
        // Arrange
        Invitation mockInvitation = createMockInvitation();

        Mockito.when(invitationRepository.get(Mockito.anyInt()))
                .thenReturn(mockInvitation);

        // Act
        Invitation result = service.get(mockInvitation.getContestId());

        // Assert
        Assertions.assertEquals(mockInvitation, result);
    }

    @Test
    public void inviteJury_Should_CallRepository() {
        User mockUser = createMockUserTwo();
        Contest mockContest = createMockContest();

        Mockito.when(invitationRepository.create(Mockito.any(Invitation.class))).thenReturn(null);
        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);

        Mockito.when(participationRepository.getAllByContest(Mockito.anyInt(), Mockito.any()))
                .thenReturn(createMockParticipationList());

        service.inviteJury(mockUser.getId(), mockContest.getContestId());

        Mockito.verify(invitationRepository, Mockito.times(1)).create(Mockito.any(Invitation.class));
    }

    @Test
    public void inviteJury_Should_Throw_WhenThereAreNoParticipations() {
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();

        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);

        Mockito.when(participationRepository.getAllByContest(Mockito.anyInt(), Mockito.any()))
                .thenReturn(createMockParticipationList());


        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.inviteJury(mockUser.getId(), mockContest.getContestId()));

    }

    @Test
    public void inviteParticipation_Should_CallRepository() {
        User mockUser = createMockUserTwo();
        Contest mockContest = createMockContest();

        Mockito.when(invitationRepository.create(Mockito.any(Invitation.class))).thenReturn(null);
        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);

        service.inviteParticipation(mockUser.getId(), mockContest.getContestId());

        Mockito.verify(invitationRepository, Mockito.times(1)).create(Mockito.any(Invitation.class));
    }

    @Test
    public void inviteParticipation_Should_Throw_WhenUserIsAdmin() {
        User mockUser = createMockOrganiser();
        Contest mockContest = createMockContest();

        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(mockUser);

        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.inviteParticipation(mockUser.getId(), mockContest.getContestId()));
    }

    @Test
    public void inviteParticipation_Should_Throw_WhenUserIsJuryMember() {
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        Set<User> juryMembers = mockContest.getJuryMembers();
        juryMembers.add(mockUser);
        mockContest.setJuryMembers(juryMembers);

        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);

        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.inviteParticipation(mockUser.getId(), mockContest.getContestId()));
    }

    @Test
    public void answerInvitationJury_Should_DeleteInvitation() {
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        Invitation mockInvitation = createMockInvitation();
        mockInvitation.setForJury(true);
        mockInvitation.setAnswer(true);

        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);
        Mockito.when(contestRepository.update(mockContest)).thenReturn(null);
        Mockito.when(invitationRepository.get(Mockito.anyInt())).thenReturn(mockInvitation);
        Mockito.when(invitationRepository.getAllByContest(Mockito.anyInt(),
                Mockito.any(FilterOptions.class))).thenReturn(createMockInvitationList());

        service.answerInvitationJury(mockUser, mockInvitation);

        Mockito.verify(invitationRepository, Mockito.times(1)).delete(mockInvitation.getInvitationId());
    }

    @Test
    public void answerInvitationJury_Should_Throw_WhenInvitationNotForJury() {
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        Invitation mockInvitation = createMockInvitation();
        mockInvitation.setForJury(false);
        mockInvitation.setAnswer(true);

        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(mockContest);

        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.answerInvitationJury(mockUser, mockInvitation));
    }

}
