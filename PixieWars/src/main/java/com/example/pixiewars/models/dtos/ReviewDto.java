package com.example.pixiewars.models.dtos;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

public class ReviewDto {

    @Min(value = 0, message = "Score must be greater or equal to 0")
    @Max(value = 10, message = "Score must be less than or equal to 10")
    private int score;

    private String comment;

    public ReviewDto() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
