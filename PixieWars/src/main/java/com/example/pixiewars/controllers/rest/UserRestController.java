package com.example.pixiewars.controllers.rest;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.helpers.UserMapper;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.Invitation;
import com.example.pixiewars.models.User;
import com.example.pixiewars.models.dtos.UserDto;
import com.example.pixiewars.services.contracts.InvitationService;
import com.example.pixiewars.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";

    private final UserService service;
    private final UserMapper userMapper;
    private final InvitationService invitationService;
    private final AuthenticationHelper authenticationHelper;

    private final FileUploadUtil fileUploadUtil;

    @Autowired
    public UserRestController(UserService service, UserMapper userMapper, InvitationService invitationService, AuthenticationHelper authenticationHelper, FileUploadUtil fileUploadUtil) {
        this.service = service;
        this.userMapper = userMapper;
        this.invitationService = invitationService;
        this.authenticationHelper = authenticationHelper;
        this.fileUploadUtil = fileUploadUtil;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return service.getAll();
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @GetMapping("/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) String username,
                             @RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) Boolean isOrganizer,
                             @RequestParam(required = false) String rank,
                             @RequestParam(required = false) String sort) {
        try {
            FilterOptionsUsers filterOptionsUsers = new FilterOptionsUsers(username, firstName, lastName, isOrganizer, rank, sort);
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            return service.filter(authenticatedUser, filterOptionsUsers);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User get(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return service.get(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(consumes = "multipart/form-data")
    public User create(@Valid @RequestPart("data") UserDto userDto, @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            User user = userMapper.fromDto(userDto);
            if (file != null) {
                user.setAvatarPhoto(fileUploadUtil.addPhoto(file));
            } else {
                user.setAvatarPhoto("https://storage.googleapis.com/pixiewars7/avatar.png");
            }
            return service.create(user);
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestPart("data") UserDto userDto,
                       @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            User user = userMapper.fromDto(id, userDto);
            if (file != null) {
                user.setAvatarPhoto(fileUploadUtil.addPhoto(file));
            }
            service.update(authenticatedUser, user);
            return user;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            service.delete(id, authenticatedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{userId}/setOrganiser")
    public User setOrganiser(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            service.makeOrganiser(authenticatedUser, userId);
            return service.get(userId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{userId}/unsetOrganiser")
    public User unsetOrganiser(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            service.demoteOrganiser(authenticatedUser, userId);
            return service.get(userId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userId}/invitations")
    public List<Invitation> getInvitations(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            return invitationService.getAllByUser(authenticatedUser, userId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{userId}/invitations/{invitationId}/accept")
    public void acceptInvitationJury(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int invitationId) {
        try {
            User authenticated = authenticationHelper.tryGetUser(headers);
            User user = service.get(userId);
            if (authenticated.getId() == user.getId()) {
                Invitation invitation = invitationService.get(invitationId);
                invitation.setAnswer(true);
                invitationService.answerInvitationJury(authenticated, invitation);
            } else {
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }

        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{userId}/invitations/{invitationId}/reject")
    public void rejectInvitationJury(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int invitationId) {
        try {
            User authenticated = authenticationHelper.tryGetUser(headers);
            User user = service.get(userId);
            if (authenticated.getId() == user.getId()) {
                Invitation invitation = invitationService.get(invitationId);
                invitation.setAnswer(false);
                invitationService.answerInvitationJury(authenticated, invitation);
            } else {
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }

        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
