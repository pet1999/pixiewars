package com.example.pixiewars.models.dtos;

import com.example.pixiewars.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.HashSet;
import java.util.Set;

public class ContestDto {
    @NotNull(message = "Title can't be empty")
    @Size(min = 5, max = 64, message = "Title should be between 5 and 64 symbols")
    private String title;
    @NotNull(message = "Description can't be empty")
    @Size(min = 5, max = 200, message = "Description should be between 5 and 200 symbols")
    private String description;

    private int categoryId;

    private boolean isPrivate;
    @Min(value = 1, message = "Hours must be greater than or equal to 1 day")
    @Max(value = 31, message = "Hours must be less than or equal to 31 days")
    private int phaseOneDays;
    @Min(value = 1, message = "Hours must be greater than or equal to 1 hour")
    @Max(value = 24, message = "Hours must be less than or equal to 24 hours")
    private int phaseTwoHours;

    private int prize;

    private String cover_url;

    @JsonIgnore
    private Set<Integer> juryInvitations;

    @JsonIgnore
    private Set<Integer> participantInvitations;

    public ContestDto() {
        this.setJuryInvitations(new HashSet<>());
        this.setParticipantInvitations(new HashSet<>());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public int getPhaseOneDays() {
        return phaseOneDays;
    }

    public void setPhaseOneDays(int phaseOneDays) {
        this.phaseOneDays = phaseOneDays;
    }

    public int getPhaseTwoHours() {
        return phaseTwoHours;
    }

    public void setPhaseTwoHours(int phaseTwoHours) {
        this.phaseTwoHours = phaseTwoHours;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Set<Integer> getJuryInvitations() {
        return juryInvitations;
    }

    public void setJuryInvitations(Set<Integer> juryInvitations) {
        this.juryInvitations = juryInvitations;
    }

    public Set<Integer> getParticipantInvitations() {
        return participantInvitations;
    }

    public void setParticipantInvitations(Set<Integer> participantInvitations) {
        this.participantInvitations = participantInvitations;
    }
}
