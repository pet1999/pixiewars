package com.example.pixiewars.controllers.mvc;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.UserMapper;
import com.example.pixiewars.models.LoginDto;
import com.example.pixiewars.models.RegisterDto;
import com.example.pixiewars.models.User;
import com.example.pixiewars.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationMvcController(UserService userService,
                                       AuthenticationHelper authenticationHelper,
                                       UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("authUser")
    public User currentUSer(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            String userEmail = (String) session.getAttribute("currentUser");
            return userService.getByEmail(userEmail);
        }
        return null;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "LoginView";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "LoginView";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getEmail(), login.getPassword());
            session.setAttribute("currentUser", user.getEmail());
            return "redirect:/contests";
        } catch (AuthorizationException e) {
            try {
                userService.getByEmail(login.getEmail());
                bindingResult.rejectValue("password", "auth_error", e.getMessage());
            } catch (EntityNotFoundException en) {
                bindingResult.rejectValue("email", "auth_error", en.getMessage());
            }
            return "LoginView";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "RegisterView";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "RegisterView";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "RegisterView";
        }

        try {
            User user = userMapper.fromDto(register);
            user.setAvatarPhoto("https://storage.googleapis.com/pixiewars7/avatar.png");
            userService.create(user);
            return "redirect:/auth/login";
        } catch (EntityDuplicateException e) {
            boolean isDuplicate = true;
            try {
                userService.get(register.getUsername());
                bindingResult.rejectValue("username", "username_error", e.getMessage());
            } catch (EntityNotFoundException ignored) {
                isDuplicate = false;
            }
            if (!isDuplicate) {
                try {
                    userService.getByEmail(register.getEmail());
                    bindingResult.rejectValue("email", "email_error", e.getMessage());
                } catch (EntityNotFoundException ignored) {
                }
            }
            return "RegisterView";
        }
    }
}