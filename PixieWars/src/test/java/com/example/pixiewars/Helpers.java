package com.example.pixiewars;

import com.example.pixiewars.models.*;
import jakarta.mail.Part;

import java.sql.Array;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

public class Helpers {

    public static User createMockOrganiser() {
        User mockUser = createMockUser();
        mockUser.setOrganiser(true);
        return mockUser;
    }

    public static User createMockOrganiserTwo() {
        User mockUser = createMockUserTwo();
        mockUser.setOrganiser(true);
        return mockUser;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setLastName("MockLastName");
        mockUser.setFirstName("MockFirstName");
        mockUser.setEmail("mock@user.com");
        mockUser.setOrganiser(false);
        mockUser.setDeleted(false);
        return mockUser;
    }

    public static User createMockUserTwo() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setUsername("MockUsername2");
        mockUser.setPassword("MockPassword2");
        mockUser.setLastName("MockLastName2");
        mockUser.setFirstName("MockFirstName2");
        mockUser.setEmail("mock2@user.com");
        mockUser.setOrganiser(false);
        mockUser.setDeleted(false);
        return mockUser;
    }

    public static Category createMockCategory(){
        var mockCategory = new Category();
        mockCategory.setCategoryId(1);
        mockCategory.setCategory_name("mockCategory");
        return mockCategory;
    }

    public static Set<User> createMockJury(){
        var mockJury = new HashSet<User>();
        mockJury.add(createMockOrganiser());
        return mockJury;
    }

    public static Contest createMockContest(){
        var mockContest = new Contest();
        mockContest.setContestId(1);
        mockContest.setTitle("mockTitle");
        mockContest.setDescription("mockDescription");
        mockContest.setCategory(createMockCategory());
        mockContest.setPrivate(false);
        LocalDateTime localDateTime1 = LocalDateTime.of(2024,10,10,10,10);
        LocalDateTime localDateTime2 = LocalDateTime.of(2025,10,10,10,10);
        ZoneId zoneId = ZoneId.systemDefault();

        mockContest.setPhase_one_time_limit(Date.from(localDateTime1.atZone(zoneId).toInstant()));
        mockContest.setPhase_two_time_limit(Date.from(localDateTime2.atZone(zoneId).toInstant()));
        mockContest.setJuryMembers(createMockJury());
        mockContest.setCoverPhoto("");
        mockContest.setChecked(false);
        return mockContest;
    }

    public static Contest createMockContestTwo(){
        var mockContest = new Contest();
        mockContest.setContestId(2);
        mockContest.setTitle("mockTitleTwo");
        mockContest.setDescription("mockDescriptionTwo");
        mockContest.setCategory(createMockCategory());
        mockContest.setPrivate(false);
        LocalDateTime localDateTime1 = LocalDateTime.of(2020,10,10,10,10);
        LocalDateTime localDateTime2 = LocalDateTime.of(2022,10,10,10,10);
        ZoneId zoneId = ZoneId.systemDefault();

        mockContest.setPhase_one_time_limit(Date.from(localDateTime1.atZone(zoneId).toInstant()));
        mockContest.setPhase_two_time_limit(Date.from(localDateTime2.atZone(zoneId).toInstant()));
        mockContest.setJuryMembers(createMockJury());
        mockContest.setCoverPhoto("");
        mockContest.setChecked(false);
        return mockContest;
    }

    public static List<Contest> createMockContestList(){
        var mockContestList = new ArrayList<Contest>();
        mockContestList.add(createMockContest());
        mockContestList.add(createMockContestTwo());
        return mockContestList;
    }

    public static Invitation createMockInvitation(){
        var mockInvitation = new Invitation();
        mockInvitation.setInvitationId(1);
        mockInvitation.setContestId(1);
        mockInvitation.setMessage("mockMessage");
        mockInvitation.setAnswer(null);
        mockInvitation.setForJury(false);
        mockInvitation.setUserId(1);
        return mockInvitation;
    }

    public static Invitation createMockInvitationTwo(){
        var mockInvitation = new Invitation();
        mockInvitation.setInvitationId(2);
        mockInvitation.setContestId(1);
        mockInvitation.setMessage("mockMessage");
        mockInvitation.setAnswer(null);
        mockInvitation.setForJury(false);
        mockInvitation.setUserId(2);
        return mockInvitation;
    }

    public static List<Invitation> createMockInvitationList(){
        var mockInvitationList = new ArrayList<Invitation>();
        mockInvitationList.add(createMockInvitation());
        mockInvitationList.add(createMockInvitationTwo());
        return mockInvitationList;
    }

    public static Participation createMockParticipation(){
        var mockParticipation = new Participation();
        mockParticipation.setId(1);
        mockParticipation.setCreator(createMockUser());
        mockParticipation.setTitle("mockTitle");
        mockParticipation.setStory("mockStory");
        mockParticipation.setContest(createMockContest());
        mockParticipation.setPhoto("mockPhoto");
        mockParticipation.setTotalScore(58);
        return mockParticipation;
    }

    public static List<Participation> createMockParticipationList(){
        var mockParticipationList = new ArrayList<Participation>();
        mockParticipationList.add(createMockParticipation());
        return mockParticipationList;
    }

    public static Review createMockReview(){
        var mockReview = new Review();
        mockReview.setId(1);
        mockReview.setParticipationId(1);
        mockReview.setJuryMember(createMockUser());
        mockReview.setScore(10);
        mockReview.setComment("comment");
        return mockReview;
    }

    public static List<Review> createMockReviewList(){
        var mockReviewList = new ArrayList<Review>();
        mockReviewList.add(createMockReview());
        return mockReviewList;
    }
}
