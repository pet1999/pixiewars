package com.example.pixiewars.helpers;

import com.example.pixiewars.models.Participation;
import com.example.pixiewars.models.Review;
import com.example.pixiewars.models.dtos.ParticipationDto;
import com.example.pixiewars.models.dtos.ReviewDto;
import com.example.pixiewars.services.contracts.ParticipationService;
import org.springframework.stereotype.Component;

@Component
public class ParticipationMapper {
    private final ParticipationService service;

    public ParticipationMapper(ParticipationService service) {
        this.service = service;
    }


    public Participation fromDto(ParticipationDto dto) { // REST create
        Participation participation = new Participation();
        participation.setTitle(dto.getTitle());
        participation.setStory(dto.getStory());
        return participation;
    }

    public Review fromDtoReview(ReviewDto dto) { // REST create
        Review review = new Review();
        review.setComment(dto.getComment());
        review.setScore(dto.getScore());
        return review;
    }
}
