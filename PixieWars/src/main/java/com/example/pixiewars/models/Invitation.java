package com.example.pixiewars.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "invitations")
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invitation_id")
    private int invitationId;


    @Column(name = "user_id")
    private int userId;


    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "message")
    private String message;

    @Column(name = "for_jury")
    private Boolean forJury;

    @JsonIgnore
    @Column(name = "answer")
    private Boolean answer;

    public Invitation() {
    }

    public int getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(int invitationId) {
        this.invitationId = invitationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getForJury() {
        return forJury;
    }

    public void setForJury(Boolean forJury) {
        this.forJury = forJury;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invitation that = (Invitation) o;
        return invitationId == that.invitationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(invitationId);
    }
}
