package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;

import java.util.List;

public interface ContestRepository {
    List<Contest> getAll();

    List<Contest> filter(FilterOptionsContests filterOptionsContests);

    Contest get(int id);

    Contest get(String title);

    Contest create(Contest contest);

    Contest update(Contest contest);

    void delete(int contestId);

}
