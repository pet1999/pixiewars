package com.example.pixiewars.models.dtos;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CategoryDto {
    @NotNull(message = "Category name can't be empty")
    @Size(min = 3, max = 20, message = "Category name should be between 3 and 20 symbols.")
    private String categoryName;

    public CategoryDto() {
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
