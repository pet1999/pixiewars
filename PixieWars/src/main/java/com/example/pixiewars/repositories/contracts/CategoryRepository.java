package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAll();

    Category get(int categoryId);

    Category getByName(String categoryName);

    Category create(Category category);

    void delete(int categoryId);

}
