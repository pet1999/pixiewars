package com.example.pixiewars.models;

import java.util.Optional;

public class FilterOptions {

    // CONTESTS - List and filter by title, category, type and phase (must)
    // PHOTOS - List and search by title (must)
    // USERS -  List and search by username, first name or last name (must)


    // Participation filter
    private Optional<String> title;

    // Invitation filter
    private Optional<Boolean> forJury;

    // Sort
    private Optional<String> sort;

    public FilterOptions() {
        this(null, null, null);
    }


    public FilterOptions(String title, Boolean forJury, String sort) {


        this.title = Optional.ofNullable(title);

        this.forJury = Optional.ofNullable(forJury);

        this.sort = Optional.ofNullable(sort);
    }

    public FilterOptions(Boolean forJury) {
        this.forJury = Optional.ofNullable(forJury);
    }

    public Optional<String> getPhotoTitle() {
        return title;
    }

    public Optional<Boolean> getForJury() {
        return forJury;
    }

    public Optional<String> getSort() {
        return sort;
    }
}
