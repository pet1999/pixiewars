![](https://storage.googleapis.com/pixiewars7/MicrosoftTeams-image.png)

*Unleash Your Inner Photographer*


# _Project Description_
PixieWars is a web application designed for photographers to easily manage and participate in online photo
contests. The application has two main parts:

- Organizational
This section of the application is for the owners to organize photo contests. They can create new contests, set contest
rules, and manage submissions. Contest owners can also view and moderate the submitted photos, and select winners.

- For Photo Junkies
This section of the application is for everyone who wants to participate in the photo contests. Users can register,
upload their photos, and join contests. Contest participants can also view other submitted photos and vote for their
favorites. Users with a certain ranking can be invited to be jury members, and help select the winners.

# _Link to Swagger Documentation_

The PixieWars app's API is documented using Swagger and the documentation can be accessed at the following
link: https://app.swaggerhub.com/apis-docs/pet1999/pixie-wars/v0.
The Swagger documentation provides all the necessary information to communicate with the InForCE API and can be used as
a reference for developers who wish to integrate with the platform.

# _How to install_
- step 1: Clone the project on your computer 
- step 2: Open the project
- step 3: Follow "Create and Fill Database with Data"
- step 4: Run the project
- step 5: Go to your browser and write "http://localhost:8080/"
- step 6: Open database find a user (The password for all users: 12345678)
- step 7: Enjoy the Pixie Wars

# _Create and Fill Database with Data_

To create a database that meets the requirements of the application, you'll need to run the 'create.sql' script found in
the 'db' folder of the main directory. This script includes the necessary SQL commands to set up the database schema,
tables, and columns. Once the 'create.sql' script has finished running, you can then execute the 'insert.sql' script
located in the same directory. This script contains SQL commands to add mock data to the database, allowing you to test
and develop the application using realistic data. It's important to note that running the 'create.sql' script before
the 'insert.sql' script is crucial, as the latter relies on the database schema and tables being set up first.

# _Images of Database Relations_

You can view a diagram showing the relationships between the different entities in the database by visiting the
following link: https://imgur.com/a/6MOsgMW.

# _Video Presentation of the Project_
You can watch the video here:
https://youtu.be/u4qWVyHKgBU

# _Images of the Project_

You can view images of the project by visiting the following link: https://imgur.com/a/AKv28wL.

# _Plan for realisation of the Project_
https://excalidraw.com/#json=Dw-s4UESJWRadkcM0FAyc,p41y3ZF_IPf7VlK3cebkdQ

# _Project Main Features_

- User registration and login
- Organisers creating categories and contests for users to participate
- Users participating in photo contests
- Organisers inviting users to participate in private contests
- Organisers inviting users to be jury in photo contests
- User profile management
- Participation creation and deletion
- Photo slider on the home page with winners photos
- Filtering functionality for contests by title, category and type
- Filtering functionality for users by username, firstname and rank
- Organisers can make organiser/demote organiser another user
- User gain points by participating in contests and for being in the first three positions of a contest
- Every user has a rank depending on its points
