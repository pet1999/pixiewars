package com.example.pixiewars.services.contracts;

import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Participation;
import com.example.pixiewars.models.Review;
import com.example.pixiewars.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ParticipationService {

    List<Participation> getAll(Integer limit);
    List<Participation> getAllByContest(int contestId, FilterOptions filterOptions);

    List<Participation> getAllByUser(int userId);

    Participation get(int id, int participationId);

    Participation create(int id, Participation participation, User user, MultipartFile file);

    void delete(int contestId, int participationId, User user);

    List<Review> getReviews(int contestId, int participationId);

    Review getReview(int contestId, int participationId, int reviewId);

    Review addReview(User user, Review review, int contestId, int participationId);

    void deleteReview(User user, int reviewId, int contestId, int participationId);
}
