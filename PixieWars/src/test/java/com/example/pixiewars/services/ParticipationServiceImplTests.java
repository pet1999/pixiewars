package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.models.*;
import com.example.pixiewars.repositories.contracts.*;
import com.example.pixiewars.services.contracts.InvitationService;
import jakarta.servlet.http.Part;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Set;

import static com.example.pixiewars.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParticipationServiceImplTests {
    @Mock
    ContestRepository contestRepository;

    @Mock
    InvitationRepository invitationRepository;

    @Mock
    InvitationService invitationService;


    @Mock
    ParticipationRepository participationRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    ReviewRepository reviewRepository;

    @Mock
    FileUploadUtil fileUploadUtil;

    @InjectMocks
    ParticipationServiceImpl service;

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(participationRepository.getAll(Mockito.anyInt())).thenReturn(null);

        // Act
        service.getAll(Mockito.anyInt());

        Mockito.verify(participationRepository, Mockito.times(1)).getAll(Mockito.anyInt());
    }

    @Test
    void getAllByContest_Should_CallRepository() {
        Contest contest = createMockContest();
        // Arrange
        Mockito.when(contestRepository.get(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(participationRepository.getAllByContest(Mockito.anyInt(),
                Mockito.any(FilterOptions.class))).thenReturn(null);

        // Act
        int contestId = 1;
        FilterOptions filterOptions = new FilterOptions();
        service.getAllByContest(contestId, filterOptions);

        // Assert
        Mockito.verify(participationRepository, Mockito.times(1))
                .getAllByContest(Mockito.anyInt(), Mockito.any(FilterOptions.class));
    }

    @Test
    void getAllByUser_Should_CallRepository() {
        User authenticatedUser = createMockUser();
        // Arrange
        Mockito.when(userRepository.get(Mockito.anyInt())).thenReturn(authenticatedUser);
        Mockito.when(participationRepository.getAllByUser(Mockito.anyInt()))
                .thenReturn(null);

        // Act
        service.getAllByUser(authenticatedUser.getId());

        // Assert
        Mockito.verify(participationRepository, Mockito.times(1))
                .getAllByUser(Mockito.anyInt());
    }

    @Test
    public void get_Should_ReturnInvitation_When_MatchByIdExist() {
        // Arrange
        Participation mockParticipation = createMockParticipation();

        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(mockParticipation);

        // Act
        Participation result = service.get(mockParticipation.getContest().getContestId(), mockParticipation.getId());

        // Assert
        Assertions.assertEquals(mockParticipation, result);
    }

    @Test
    public void create_Should_CallRepository() {
        // Arrange
        User authenticatedUser = createMockUserTwo();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(mockContest.getContestId()))
                .thenReturn(mockContest);

        // Act
        service.create(mockContest.getContestId(), mockParticipation, authenticatedUser, file);

        // Assert
        Mockito.verify(participationRepository, Mockito.times(1))
                .create(mockParticipation);
    }

    @Test
    public void create_Should_Throw_WhenPhaseOneIsFinished() {
        // Arrange
        User authenticatedUser = createMockUserTwo();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Participation mockParticipation = createMockParticipation();
        LocalDateTime localDateTime2 = LocalDateTime.of(2021, 10, 10, 10, 10);
        ZoneId zoneId = ZoneId.systemDefault();
        mockContest.setPhase_one_time_limit(Date.from(localDateTime2.atZone(zoneId).toInstant()));

        Mockito.when(contestRepository.get(mockContest.getContestId()))
                .thenReturn(mockContest);

        // Act

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.create(mockContest.getContestId(), mockParticipation, authenticatedUser, file));

    }

    @Test
    public void create_Should_Throw_WhenUserIsJury() {
        // Arrange
        User authenticatedUser = createMockUser();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(mockContest.getContestId()))
                .thenReturn(mockContest);

        // Act

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.create(mockContest.getContestId(), mockParticipation, authenticatedUser, file));

    }

    @Test
    public void create_Should_Throw_WhenFileIsNull() {
        // Arrange
        User authenticatedUser = createMockUserTwo();
        Contest mockContest = createMockContest();
        MultipartFile file = null;
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(mockContest.getContestId()))
                .thenReturn(mockContest);

        // Act

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.create(mockContest.getContestId(), mockParticipation, authenticatedUser, file));

    }

    @Test
    public void create_Should_CallRepository_WhenContestIsPrivate() {
        // Arrange
        User authenticatedUser = createMockUserTwo();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Participation mockParticipation = createMockParticipation();
        mockContest.setPrivate(true);

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(invitationService.getAllByContest(Mockito.anyInt(),
                Mockito.any())).thenReturn(createMockInvitationList());

        // Act
        service.create(mockContest.getContestId(), mockParticipation, authenticatedUser, file);

        // Assert
        Mockito.verify(participationRepository, Mockito.times(1))
                .create(mockParticipation);
    }

    @Test
    void delete_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);

        service.delete(1, 1, authenticatedUser);

        // Assert
        Mockito.verify(participationRepository, Mockito.times(1))
                .delete(1);
    }

    @Test
    void getReviews_Should_CallRepository() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.getAll(Mockito.anyInt()))
                .thenReturn(null);

        service.getReviews(1, 1);

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .getAll(Mockito.anyInt());
    }

    @Test
    void getReview_Should_CallRepository() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.get(Mockito.anyInt()))
                .thenReturn(null);

        service.getReview(1, 1, 1);

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .get(Mockito.anyInt());
    }

    @Test
    void addReview_Should_CallRepository() {
        // Arrange
        User authenticatedUser = createMockOrganiserTwo();
        Contest mockContest = createMockContest();
        Set<User> juryMembers = mockContest.getJuryMembers();
        juryMembers.add(authenticatedUser);
        mockContest.setJuryMembers(juryMembers);
        Participation mockParticipation = createMockParticipation();
        Review mockReview = createMockReview();
//        LocalDateTime localDateTime2 = LocalDateTime.of(2022,10,10,10,10);
//        ZoneId zoneId = ZoneId.systemDefault();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.getAll(Mockito.anyInt()))
                .thenReturn(createMockReviewList());
        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(createMockParticipation());

        service.addReview(authenticatedUser, mockReview, mockContest.getContestId(), mockParticipation.getId());

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .create(mockReview);
    }

    @Test
    void addReview_Should_Throw_WhenContestIsFinished() {
        // Arrange
        User authenticatedUser = createMockOrganiserTwo();
        Contest mockContest = createMockContest();
        Set<User> juryMembers = mockContest.getJuryMembers();
        juryMembers.add(authenticatedUser);
        mockContest.setJuryMembers(juryMembers);
        Participation mockParticipation = createMockParticipation();
        Review mockReview = createMockReview();
        LocalDateTime localDateTime2 = LocalDateTime.of(2022,10,10,10,10);
        ZoneId zoneId = ZoneId.systemDefault();
        mockContest.setPhase_two_time_limit(Date.from(localDateTime2.atZone(zoneId).toInstant()));

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.getAll(Mockito.anyInt()))
                .thenReturn(createMockReviewList());
        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(createMockParticipation());

        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.addReview(authenticatedUser, mockReview, mockContest.getContestId(), mockParticipation.getId()));

    }

    @Test
    void addReview_Should_Throw_UserIsNotJury() {
        // Arrange
        User authenticatedUser = createMockOrganiserTwo();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        Review mockReview = createMockReview();


        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.getAll(Mockito.anyInt()))
                .thenReturn(createMockReviewList());
        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(createMockParticipation());


        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.addReview(authenticatedUser, mockReview, mockContest.getContestId(), mockParticipation.getId()));

    }

    @Test
    void addReview_Should_Throw_UserAlreadyReviewed() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        Review mockReview = createMockReview();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.getAll(Mockito.anyInt()))
                .thenReturn(createMockReviewList());
        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(createMockParticipation());


        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.addReview(authenticatedUser, mockReview, mockContest.getContestId(), mockParticipation.getId()));

    }

    @Test
    void deleteReview_Should_CallRepository() {
        // Arrange
        User authenticatedUser = createMockOrganiserTwo();
        Contest mockContest = createMockContest();
        Set<User> juryMembers = mockContest.getJuryMembers();
        juryMembers.add(authenticatedUser);
        mockContest.setJuryMembers(juryMembers);
        Participation mockParticipation = createMockParticipation();
        Review mockReview = createMockReview();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);
        Mockito.when(reviewRepository.get(Mockito.anyInt()))
                .thenReturn(mockReview);
        Mockito.when(participationRepository.get(Mockito.anyInt()))
                .thenReturn(createMockParticipation());

        service.deleteReview(authenticatedUser,mockReview.getId(), mockContest.getContestId(), mockParticipation.getId());

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .delete(mockReview.getId());
    }

}
