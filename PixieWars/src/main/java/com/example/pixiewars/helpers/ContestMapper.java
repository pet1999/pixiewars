package com.example.pixiewars.helpers;


import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.dtos.ContestDto;
import com.example.pixiewars.services.contracts.CategoryService;
import com.example.pixiewars.services.contracts.ContestService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;

@Component
public class ContestMapper {

    private final ContestService contestService;
    private final CategoryService categoryService;

    @Autowired
    public ContestMapper(ContestService contestService, CategoryService categoryService) {
        this.contestService = contestService;
        this.categoryService = categoryService;
    }

    public Contest fromDto(int id, ContestDto dto) { // REST update
        Contest contest = fromDto(dto);
        contest.setContestId(id);
        Contest repositoryContest = contestService.get(id);
        contest.setPhase_one_time_limit(repositoryContest.getPhase_one_time_limit());
        contest.setPhase_two_time_limit(repositoryContest.getPhase_two_time_limit());
        contest.setCoverPhoto(repositoryContest.getCoverPhoto());
        contest.setJuryMembers(repositoryContest.getJuryMembers());
        return contest;
    }

    public Contest fromDto(ContestDto dto) { // REST create
        Contest contest = new Contest();
        contest.setTitle(dto.getTitle());
        contest.setDescription(dto.getDescription());
        contest.setCategory(categoryService.get(dto.getCategoryId()));
        contest.setPrivate(dto.isPrivate());
        LocalDateTime currentDate = LocalDateTime.now();
        Date date = Date.from(currentDate.atZone(ZoneId.systemDefault()).toInstant());
        Date phaseOneTimeLimit = DateUtils.addDays(date, dto.getPhaseOneDays());
        contest.setPhase_one_time_limit(phaseOneTimeLimit);
        contest.setPhase_two_time_limit(DateUtils.addHours(phaseOneTimeLimit, dto.getPhaseTwoHours()));
        contest.setPrize(dto.getPrize());
        contest.setCoverPhoto(dto.getCover_url());
        contest.setJuryMembers(new HashSet<>());
        return contest;
    }

    public ContestDto toDto(Contest contest1) {
        ContestDto contestDto = new ContestDto();
        contestDto.setTitle(contest1.getTitle());
        contestDto.setDescription(contest1.getDescription());
        contestDto.setCategoryId(contest1.getCategory().getCategoryId());
        contestDto.setPrivate(contest1.isPrivate());
        contestDto.setPhaseOneDays(1);
        contestDto.setPhaseTwoHours(1);
        contestDto.setPrize(contest1.getPrize());
        contestDto.setCover_url(contest1.getCoverPhoto());
        return contestDto;
    }
}
