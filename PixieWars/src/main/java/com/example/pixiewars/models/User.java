package com.example.pixiewars.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User implements Comparable<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "username")
    private String username;
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "points")
    private int points;
    @Column(name = "rank")
    private String rank;

    @Column(name = "avatar_photo")
    private String avatarPhoto;

    @JsonIgnore
    @Column(name = "is_organizer")
    private boolean isOrganiser;
    @JsonIgnore
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JsonIgnore
    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "token_expiry_date")
    private Date tokenExpiryDate;


    @Column(name = "contest_wins")
    private int contestWins;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        updateRank(points);
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getAvatarPhoto() {
        return avatarPhoto;
    }

    public void setAvatarPhoto(String avatarPhoto) {
        this.avatarPhoto = avatarPhoto;
    }

    @JsonIgnore
    public boolean isOrganiser() {
        return isOrganiser;
    }

    public void setOrganiser(boolean organiser) {
        if(organiser){
            setRank("Organiser");
        }else {
            setPoints(getPoints());
        }
        isOrganiser = organiser;
    }

    @JsonIgnore
    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @JsonIgnore
    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public int getContestWins() {
        return contestWins;
    }

    public void setContestWins(int contestWins) {
        this.contestWins = contestWins;
    }

    public Date getTokenExpiryDate() {
        return tokenExpiryDate;
    }

    public void setTokenExpiryDate(Date tokenExpiryDate) {
        this.tokenExpiryDate = tokenExpiryDate;
    }

    public void updateRank(int points) {
        if (points <= 50) {
            setRank("Junkie");
        } else if (points <= 150) {
            setRank("Enthusiast");
        } else if (points <= 1000) {
            setRank("Master");
        } else {
            setRank("Dictator");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(User compareUser) {
        int compareUserPoints = compareUser.getPoints();

        //descending order
        return compareUserPoints - this.points;
    }
}
