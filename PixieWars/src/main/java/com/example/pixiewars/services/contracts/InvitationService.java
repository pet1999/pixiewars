package com.example.pixiewars.services.contracts;

import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Invitation;
import com.example.pixiewars.models.User;

import java.util.List;

public interface InvitationService {

    List<Invitation> getAllByContest(int contestId, FilterOptions filterOptions);

    List<Invitation> getAllByUser(User authenticated, int userId);

    Invitation get(int invitationId);

    Invitation inviteJury(int userId, int contestId);

    Invitation inviteParticipation(int userId, int contestId);

    void answerInvitationJury(User authenticated, Invitation invitation);

    void delete(int invitationId);
}
