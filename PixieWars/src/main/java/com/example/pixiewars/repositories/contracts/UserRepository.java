package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

    List<User> filter(FilterOptionsUsers filterOptionsUsers);

    User get(int id);

    User get(String username);

    User getByEmail(String email);

    User getByToken(String token);

    User create(User user);

    void update(User user);

}
