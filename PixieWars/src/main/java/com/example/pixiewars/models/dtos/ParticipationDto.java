package com.example.pixiewars.models.dtos;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ParticipationDto {

    @NotNull(message = "Title can't be empty")
    @Size(min = 5, max = 64, message = "Title should be between 5 and 64 symbols")
    private String title;
    @NotNull(message = "Story can't be empty")
    @Size(min = 5, max = 200, message = "Story should be between 5 and 200 symbols")
    private String story;

    public ParticipationDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
