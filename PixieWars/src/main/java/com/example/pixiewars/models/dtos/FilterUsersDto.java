package com.example.pixiewars.models.dtos;

public class FilterUsersDto {

    private String username;
    private String firstName;
    private String lastName;
    private Boolean isOrganiser;
    private String rank;
    private String sort;

    public FilterUsersDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getOrganizer() {
        return isOrganiser;
    }

    public void setOrganizer(Boolean organizer) {
        isOrganiser = organizer;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
