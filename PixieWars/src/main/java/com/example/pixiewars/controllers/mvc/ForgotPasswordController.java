package com.example.pixiewars.controllers.mvc;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.Utility;
import com.example.pixiewars.models.User;
import com.example.pixiewars.services.contracts.UserService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.UnsupportedEncodingException;

@Controller
public class ForgotPasswordController {
    private JavaMailSender mailSender;
    private UserService userService;

    @Autowired
    public ForgotPasswordController(JavaMailSender mailSender, UserService userService) {
        this.mailSender = mailSender;
        this.userService = userService;
    }

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "Forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomStringUtils.randomAlphabetic(30);
        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. You have 1 hour. Please check.");
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }
        return "Forgot_password_form";
    }

    public void sendEmail(String recipientEmail, String link)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom("pixiewarspd@gmail.com", "PixieWars Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<html><head><style>"
                + "/* Tailwind CSS */"
                + "@import url('https://unpkg.com/tailwindcss@2.2.17/dist/tailwind.min.css');"
                + "</style></head><body>"
                + "<div class=\"font-sans\">"
                + "<p class=\"text-lg\">Hello,</p>"
                + "<p class=\"text-lg\">You have requested to reset your password.</p>"
                + "<p class=\"text-lg\">Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\" class=\"inline-block px-4 py-2 rounded-lg bg-blue-600 text-white text-lg no-underline\">Change my password</a></p>"
                + "<br>"
                + "<p class=\"text-lg\">Ignore this email if you remember your password, "
                + "or if you have not made the request.</p>"
                + "</div>"
                + "</body></html>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }


    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        try {
            userService.getByResetPasswordToken(token);
            model.addAttribute("token", token);
            return "Reset_password_form";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        try {
            String token = request.getParameter("token");
            String password = request.getParameter("password");

            User user = userService.getByResetPasswordToken(token);
            model.addAttribute("title", "Reset your password");
            userService.updatePassword(user, password);
            model.addAttribute("message", "You have successfully changed your password.");
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }


    }
}
