package com.example.pixiewars.models.dtos;

public class FilterPhotosDto {

    private String photoTitle;

    private String sort;


    public FilterPhotosDto() {
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
