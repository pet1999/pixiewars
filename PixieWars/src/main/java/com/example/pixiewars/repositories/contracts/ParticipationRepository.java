package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Participation;

import java.util.List;

public interface ParticipationRepository {

    List<Participation> getAll(Integer limit);
    List<Participation> getAllByContest(int contestId, FilterOptions filterOptions);

    List<Participation> getAllByUser(int userId);

    Participation get(int participationId);

    Participation get(String title);

    Participation create(Participation participation);

    Participation update(Participation participation);

    void delete(int participationId);
}
