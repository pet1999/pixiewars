package com.example.pixiewars.controllers.rest;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.ParticipationMapper;
import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Participation;
import com.example.pixiewars.models.User;
import com.example.pixiewars.models.dtos.ParticipationDto;
import com.example.pixiewars.services.contracts.ParticipationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/contests/{id}/participations")
public class ParticipationRestController {

    private final ParticipationService participationService;
    private final ParticipationMapper participationMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParticipationRestController(ParticipationService participationService,
                                       ParticipationMapper participationMapper,
                                       AuthenticationHelper authenticationHelper) {
        this.participationService = participationService;
        this.participationMapper = participationMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Participation> getAllByContest(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                               @RequestParam(required = false) String title,
                                               @RequestParam(required = false) String sort) {
        try {
            authenticationHelper.tryGetUser(headers);
            FilterOptions filterOptions = new FilterOptions(title, null, sort);
            return participationService.getAllByContest(id, filterOptions);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{participationId}")
    public Participation get(@RequestHeader HttpHeaders headers,
                             @PathVariable int id, @PathVariable int participationId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return participationService.get(id, participationId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(consumes = "multipart/form-data")
    public Participation create(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                @Valid @RequestPart("data") ParticipationDto dto,
                                @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Participation participation = participationMapper.fromDto(dto);
            return participationService.create(id, participation, user, file);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{participationId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable int participationId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            participationService.delete(id, participationId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
