package com.example.pixiewars.controllers.mvc;


import com.example.pixiewars.services.contracts.ContestService;
import com.example.pixiewars.services.contracts.ParticipationService;
import com.example.pixiewars.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final ParticipationService participationService;
    private final ContestService contestService;

    public HomeMvcController(UserService userService, ParticipationService participationService, ContestService contestService) {
        this.userService = userService;
        this.participationService = participationService;
        this.contestService = contestService;
    }


    @GetMapping
    public String showHomePage(Model model) {
        int sizeAllUsers = userService.getAll().size();
        int sizeAllParticipations = participationService.getAll(null).size();
        int sizeAllContests = contestService.getAll().size();

        model.addAttribute("usersSize", sizeAllUsers);
        model.addAttribute("contestsSize", sizeAllContests);
        model.addAttribute("participationsSize", sizeAllParticipations);
        model.addAttribute("pictures", participationService.getAll(10));

        return "HomeView";
    }
}
