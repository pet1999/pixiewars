package com.example.pixiewars.services;

import com.example.pixiewars.models.*;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
@Configuration
@EnableScheduling

public class ScheduledUpdateService {
    public static final int FIRST_PLACE_POINTS = 50;
    public static final int SHARED_FIRST_PLACE_POINTS = 40;

    public static final int SECOND_PLACE_POINTS = 35;
    public static final int SHARED_SECOND_PLACE_POINTS = 25;

    public static final int THIRD_PLACE_POINTS = 20;
    public static final int SHARED_THIRD_PLACE_POINTS = 10;

    public static final int TEN_MINUTES = 10 * 60 * 1000;

    private final ContestRepository contestRepository;
    private final ParticipationRepository participationRepository;
    private final UserRepository userRepository;

    private final JavaMailSender mailSender;

    @Autowired
    public ScheduledUpdateService(ContestRepository contestRepository,
                                  ParticipationRepository participationRepository,
                                  UserRepository userRepository, JavaMailSender mailSender) {
        this.contestRepository = contestRepository;
        this.participationRepository = participationRepository;
        this.userRepository = userRepository;
        this.mailSender = mailSender;
    }

    @Scheduled(fixedRate = TEN_MINUTES)
    public void updateFinishedContests() throws MessagingException, UnsupportedEncodingException {
        FilterOptionsContests filterContests = new FilterOptionsContests(
                null, null, null, false, "finished", null);

        List<Contest> allFinishedUnChecked = contestRepository.filter(filterContests);
        for (Contest contest : allFinishedUnChecked) {

            FilterOptions filterParticipations = new FilterOptions(null, null, "totalScore desc");
            List<Participation> allParticipations =
                    participationRepository.getAllByContest(contest.getContestId(), filterParticipations);
            if(!allParticipations.isEmpty()){
                sortingByPlaces(allParticipations);
            }

            contest.setChecked(true);
            contestRepository.update(contest);
        }


    }

    private void sortingByPlaces(List<Participation> allParticipations) throws MessagingException, UnsupportedEncodingException {

        List<Participation> firstPlace = new ArrayList<>();
        List<Participation> secondPlace = new ArrayList<>();
        List<Participation> thirdPlace = new ArrayList<>();



        Participation previousParticipant = allParticipations.get(0);
        firstPlace.add(previousParticipant);
        firstPlace.get(0).setPosition(1);
        participationRepository.update(firstPlace.get(0));

        int position = 1;


        for (int i = 1; i < allParticipations.size(); i++) {//2
            Participation currentParticipation = allParticipations.get(i);
            if (currentParticipation.getTotalScore() == previousParticipant.getTotalScore()) {
                if (position == 1) {
                    firstPlace.add(currentParticipation);
                    currentParticipation.setPosition(position);
                } else if (position == 2) {
                    secondPlace.add(currentParticipation);
                    currentParticipation.setPosition(position);
                } else if (position == 3){
                    thirdPlace.add(currentParticipation);
                    currentParticipation.setPosition(position);
                } else {
                    currentParticipation.setPosition(position);
                }
                previousParticipant = allParticipations.get(i);

            } else if (currentParticipation.getTotalScore() < previousParticipant.getTotalScore()) {
                position++;
                if (position == 2) {
                    secondPlace.add(currentParticipation);
                    currentParticipation.setPosition(position);
                } else if (position == 3) {
                    thirdPlace.add(currentParticipation);
                    currentParticipation.setPosition(position);
                } else {
                    currentParticipation.setPosition(position);
                }
                previousParticipant = currentParticipation;
            }
            participationRepository.update(currentParticipation);
        }
        awardParticipants(firstPlace, secondPlace, thirdPlace);
    }

    private void awardParticipants(List<Participation> firstPlace,
                                   List<Participation> secondPlace,
                                   List<Participation> thirdPlace) throws MessagingException, UnsupportedEncodingException {
        int bonusPoints = 0;

        //FIRST
        if(!secondPlace.isEmpty()){
            if (firstPlace.get(0).getTotalScore() >= (secondPlace.get(0).getTotalScore() * 2)) {
                bonusPoints = 25;
            }
        }

        if (firstPlace.size() > 1) {
            for (Participation sharedPlaceONE : firstPlace) {
                updatePoints(sharedPlaceONE, SHARED_FIRST_PLACE_POINTS,true);
                sendWinnerEmail(sharedPlaceONE.getCreator().getEmail(),sharedPlaceONE.getContest().getPrize(),firstPlace.size(),sharedPlaceONE.getContest() );
            }
        } else {
            int points = FIRST_PLACE_POINTS + bonusPoints;
            updatePoints(firstPlace.get(0), points,true);
            sendWinnerEmail(firstPlace.get(0).getCreator().getEmail(),firstPlace.get(0).getContest().getPrize(),firstPlace.size(),firstPlace.get(0).getContest());
        }

        //SECOND
        if (secondPlace.size() > 1) {
            for (Participation sharedPlaceTWO : secondPlace) {
                updatePoints(sharedPlaceTWO, SHARED_SECOND_PLACE_POINTS, false);
            }
        } else if(secondPlace.size() == 1) {
            updatePoints(secondPlace.get(0), SECOND_PLACE_POINTS, false);
        }

        //THIRD
        if (thirdPlace.size() > 1) {
            for (Participation sharedPlaceTHREE : thirdPlace) {
                updatePoints(sharedPlaceTHREE, SHARED_THIRD_PLACE_POINTS, false);
            }
        } else if(thirdPlace.size() == 1){
            updatePoints(thirdPlace.get(0), THIRD_PLACE_POINTS, false);
        }
    }


    private void updatePoints(Participation sharedPlaceONE, int points, boolean isWinner) {
        User user = sharedPlaceONE.getCreator();
        user.setPoints(user.getPoints() + points);
        if (isWinner) {
            user.setContestWins(user.getContestWins() + 1);
        }
        userRepository.update(user);
    }

    public void sendWinnerEmail(String recipientEmail, int prize, int winners,Contest contest)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom("pixiewarspd@gmail.com", "PixieWars Support");
        helper.setTo(recipientEmail);

        String subject = "Congratulations! You won a the photo contest!";

        String content = "<p>Hello,</p>"
                + "<p>You WIN the photo contest: " + contest.getTitle() +".</p>"
                + "<p>The full prize for the contest is " + prize + "$ and there are " + winners +
                ". So you win " + prize/winners + "$. </p>"
                + "<br>"
                + "<p>Greetings, "
                + "PixieWars.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }


}

