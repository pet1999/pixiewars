package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.example.pixiewars.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository contestRepository;

    @Mock
    FileUploadUtil fileUploadUtil;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    ContestServiceImpl service;

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(contestRepository.getAll()).thenReturn(null);

        // Act
        service.getAll();

        Mockito.verify(contestRepository, Mockito.times(1)).getAll();
    }

    @Test
    void get_Should_CallRepository_withFilter() {
        // Arrange
        User authenticatedUser = createMockUser();
        FilterOptionsContests filterOptionsContests = new FilterOptionsContests();
        Mockito.when(contestRepository.filter(filterOptionsContests)).thenReturn(null);

        // Act
        service.filter(filterOptionsContests);

        Mockito.verify(contestRepository, Mockito.times(1)).filter(filterOptionsContests);
    }

    @Test
    public void get_Should_ReturnContest_When_MatchByIdExist() {
        // Arrange
        Contest mockContest = createMockContest();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);

        // Act
        Contest result = service.get(mockContest.getContestId());

        // Assert
        Assertions.assertEquals(mockContest, result);
    }

    @Test
    public void create_Should_CallRepository_When_ContestWithSameTitleDoesNotExistAndCoverPhotoIsNull() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);

        Mockito.when(contestRepository.get(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockContest,authenticatedUser,file);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .create(mockContest);
    }

    @Test
    public void create_Should_CallRepository_When_ContestWithSameTitleDoesNotExist() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        mockContest.setCoverPhoto(null);
        MultipartFile file = Mockito.mock(MultipartFile.class);

        Mockito.when(contestRepository.get(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockContest,authenticatedUser,file);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .create(mockContest);
    }

    @Test
    public void create_Should_Throw_When_ContestWithSameTitleExists() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        mockContest.setCoverPhoto(null);
        MultipartFile file = Mockito.mock(MultipartFile.class);

        Mockito.when(contestRepository.get(mockContest.getTitle()))
                .thenReturn(mockContest);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> service.create(mockContest,authenticatedUser,file));
    }

    @Test
    void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);

        // Act
        service.update(mockContest, authenticatedUser,file);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    void update_Should_Throw_When_TitleIsDifferent() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Contest mockContest2 = createMockContestTwo();
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest2);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(mockContest,authenticatedUser,file));
    }

    @Test
    void update_Should_Throw_When_PhaseOneTimeLimitIsDifferent() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Contest mockContest2 = createMockContestTwo();
        mockContest2.setTitle(mockContest.getTitle());
        LocalDateTime localDateTime = LocalDateTime.of(2021,10,10,10,10);
        ZoneId zoneId = ZoneId.systemDefault();
        mockContest2.setPhase_one_time_limit(Date.from(localDateTime.atZone(zoneId).toInstant()));
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest2);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(mockContest,authenticatedUser,file));
    }

    @Test
    void update_Should_Throw_When_PhaseTwoTimeLimitIsDifferent() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();
        Contest mockContest2 = createMockContestTwo();
        mockContest2.setTitle(mockContest.getTitle());
        LocalDateTime localDateTime = LocalDateTime.of(2021,10,10,10,10);
        ZoneId zoneId = ZoneId.systemDefault();
        mockContest2.setPhase_two_time_limit(Date.from(localDateTime.atZone(zoneId).toInstant()));
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest2);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.update(mockContest,authenticatedUser,file));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User authenticatedUser = createMockOrganiser();
        Contest mockContest = createMockContest();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);

        // Act
        service.delete(1,authenticatedUser);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .delete(1);
    }

    @Test
    void delete_Should_Throw_When_UserIsNotAdmin() {
        // Arrange
        User authenticatedUser = createMockUser();
        Contest mockContest = createMockContest();

        Mockito.when(contestRepository.get(Mockito.anyInt()))
                .thenReturn(mockContest);

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.delete(1,authenticatedUser));
    }
}
