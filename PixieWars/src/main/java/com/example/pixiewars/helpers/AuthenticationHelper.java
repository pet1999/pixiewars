package com.example.pixiewars.helpers;


import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.User;
import com.example.pixiewars.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String INVALID_AUTHENTICATION_ERROR = "Invalid authentication.";
    private static final String LOGIN_FAILURE_MESSAGE = "N0 logged in user";
    private static final String EMAIL_NOT_EXISTING = "Wrong email!";
    private static final String PASSWORD_NOT_EXISTING = "Wrong password!";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }

        try {
            String userInfo = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            String email = getEmail(userInfo);

            User user = userService.getByEmail(email);
            return user;

        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }
    }

    public User tryGetCurrentUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthorizationException(LOGIN_FAILURE_MESSAGE);
        }

        return userService.getByEmail(currentUser);
    }

    public User verifyAuthentication(String email, String password) {
        try {
            User user = userService.getByEmail(email);
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!encoder.matches(password,user.getPassword())) {
                throw new AuthorizationException(PASSWORD_NOT_EXISTING);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(EMAIL_NOT_EXISTING);
        }
    }

    private String getEmail(String userInfo) {
        if (userInfo.isEmpty()) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }

        return userInfo;
    }

}
