package com.example.pixiewars.services;

import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.Invitation;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.InvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Configuration
@EnableScheduling
public class ScheduledDeleteInvitationsService {
    public static final int TEN_MINUTES = 10 * 60 * 1000;
    public static final int TWENTY_MIN = 20 * 60 * 1000;

    private final ContestRepository contestRepository;
    private final InvitationRepository invitationRepository;
    @Autowired
    public ScheduledDeleteInvitationsService(ContestRepository contestRepository, InvitationRepository invitationRepository) {
        this.contestRepository = contestRepository;
        this.invitationRepository = invitationRepository;
    }

    @Scheduled(fixedRate = TEN_MINUTES)
    public void deleteActiveInvitations() {
        FilterOptionsContests fo = new FilterOptionsContests(null,null,
                null,null,"phase_one",null);
        List<Contest> allContestsPhaseOne = contestRepository.filter(fo);

        for (Contest contest : allContestsPhaseOne) {
            if(isContestEndingSoon(contest.getPhase_one_time_limit())){
                List<Invitation> allActiveInvitations = invitationRepository.getAllByContest(contest.getContestId(), new FilterOptions());
                for (Invitation activeInvitation : allActiveInvitations) {
                    invitationRepository.delete(activeInvitation.getInvitationId());
                }
            }
        }
    }

    public static boolean isContestEndingSoon(Date finishDate) {
        Date currentTime = new Date();
        Date twentyMinutesBeforeFinishDate = new Date(finishDate.getTime() - TWENTY_MIN);
        return currentTime.after(twentyMinutesBeforeFinishDate);
    }
}
