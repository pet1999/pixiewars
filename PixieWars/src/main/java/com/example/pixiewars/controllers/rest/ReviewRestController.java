package com.example.pixiewars.controllers.rest;


import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.ParticipationMapper;
import com.example.pixiewars.models.Review;
import com.example.pixiewars.models.User;
import com.example.pixiewars.models.dtos.ReviewDto;
import com.example.pixiewars.services.contracts.ParticipationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/contests/{contestId}/participations/{participationId}/reviews")
public class ReviewRestController {

    private final ParticipationService participationService;
    private final ParticipationMapper participationMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ReviewRestController(ParticipationService participationService, ParticipationMapper participationMapper, AuthenticationHelper authenticationHelper) {
        this.participationService = participationService;
        this.participationMapper = participationMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Review> getReviews(@RequestHeader HttpHeaders headers,
                                   @PathVariable int contestId, @PathVariable int participationId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return participationService.getReviews(contestId, participationId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{reviewId}")
    public Review getReview(@RequestHeader HttpHeaders headers,
                            @PathVariable int reviewId,
                            @PathVariable int contestId, @PathVariable int participationId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return participationService.getReview(contestId, participationId, reviewId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping
    public Review addReview(@RequestHeader HttpHeaders headers, @Valid @RequestBody ReviewDto reviewDto,
                            @PathVariable int contestId, @PathVariable int participationId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Review review = participationMapper.fromDtoReview(reviewDto);
            return participationService.addReview(user, review, contestId, participationId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{reviewId}")
    public void deleteReview(@RequestHeader HttpHeaders headers, @PathVariable int reviewId,
                             @PathVariable int contestId, @PathVariable int participationId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            participationService.deleteReview(user, reviewId, contestId, participationId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
