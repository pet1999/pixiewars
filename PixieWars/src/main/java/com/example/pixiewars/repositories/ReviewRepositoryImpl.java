package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Review;
import com.example.pixiewars.repositories.contracts.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReviewRepositoryImpl implements ReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Review> getAll(int participationId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> list = session.createQuery(" from Review where " +
                    " participationId = :participationId");
            list.setParameter("participationId", participationId);
            return list.list();
        }
    }

    @Override
    public Review get(int id) {
        try (Session session = sessionFactory.openSession()) {
            Review review = session.get(Review.class, id);
            if (review == null) {
                throw new EntityNotFoundException("Review", id);
            }
            return review;
        }
    }

    @Override
    public Review create(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(review);
            session.getTransaction().commit();
            return review;
        }
    }

    @Override
    public void delete(int reviewId) {
        Review reviewToDelete = get(reviewId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(reviewToDelete);
            session.getTransaction().commit();
        }
    }
}
