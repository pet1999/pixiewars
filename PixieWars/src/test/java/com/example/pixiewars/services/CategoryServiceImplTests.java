package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Category;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.CategoryRepository;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static com.example.pixiewars.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    CategoryRepository mockCategoryRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(mockCategoryRepository.getAll()).thenReturn(null);

        // Act
        service.get();

        Mockito.verify(mockCategoryRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void get_Should_ReturnCategory_When_MatchByIdExist() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(mockCategoryRepository.get(Mockito.anyInt()))
                .thenReturn(mockCategory);

        // Act
        Category result = service.get(mockCategory.getCategoryId());

        // Assert
        Assertions.assertEquals(mockCategory, result);
    }

    @Test
    public void get_Should_ReturnCategory_When_MatchByCategoryNameExist() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(mockCategoryRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCategory);

        // Act
        Category result = service.getByName(mockCategory.getCategory_name());

        // Assert
        Assertions.assertEquals(mockCategory, result);
    }

    @Test
    public void create_Should_CallRepository_When_CategoryWithSameNameDoesNotExist() {
        // Arrange
        Category mockCategory = createMockCategory();
        User authenticatedUser = createMockOrganiser();


        Mockito.when(mockCategoryRepository.getByName(mockCategory.getCategory_name()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(authenticatedUser,mockCategory);

        // Assert
        Mockito.verify(mockCategoryRepository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    public void create_Should_Throw_When_CategoryWithSameNameDoesExist() {
        // Arrange
        Category mockCategory = createMockCategory();
        User authenticatedUser = createMockOrganiser();


        Mockito.when(mockCategoryRepository.getByName(mockCategory.getCategory_name()))
                .thenReturn(mockCategory);

        // Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> service.create(authenticatedUser,mockCategory));
    }

    @Test
    public void create_Should_Throw_When_UserIsNotAdmin() {
        // Arrange
        Category mockCategory = createMockCategory();
        User authenticatedUser = createMockUser();

        // Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> service.create(authenticatedUser,mockCategory));
    }
}
