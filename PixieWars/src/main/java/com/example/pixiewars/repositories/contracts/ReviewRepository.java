package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.Review;

import java.util.List;

public interface ReviewRepository {
    List<Review> getAll(int participationId);

    Review get(int id);

    Review create(Review review);

    void delete(int reviewId);

}
