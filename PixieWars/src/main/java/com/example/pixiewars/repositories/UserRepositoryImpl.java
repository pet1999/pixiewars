package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where isDeleted = false", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> filter(FilterOptionsUsers filterOptionsUsers) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filter.add(" isDeleted = :isDeleted ");
            queryParams.put("isDeleted", false);

            filterOptionsUsers.getUsername().ifPresent(v -> {
                filter.add(" username like :username ");
                queryParams.put("username", "%" + v + "%");
            });
            filterOptionsUsers.getFirstName().ifPresent(v -> {
                filter.add(" firstName like :firstName ");
                queryParams.put("firstName", "%" + v + "%");
            });
            filterOptionsUsers.getLastName().ifPresent(v -> {
                filter.add(" lastName like :lastName ");
                queryParams.put("lastName", "%" + v + "%");
            });
            filterOptionsUsers.getIsOrganiser().ifPresent(v -> {
                filter.add(" isOrganiser = :isOrganiser ");
                queryParams.put("isOrganiser", v);
            });
            filterOptionsUsers.getRank().ifPresent(value -> {
                filter.add("rank like :rank");
                queryParams.put("rank", "%" + value + "%");
            });

            queryString.append(" where ").append(String.join(" and ", filter));

            queryString.append(generateSortBy(filterOptionsUsers));

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);

            return queryList.list();
        }
    }

    private String generateSortBy(FilterOptionsUsers filterOptionsUsers) {
        StringBuilder queryString = new StringBuilder(" order by ");

        if (filterOptionsUsers.getSort().isEmpty()) {
            return "";
        }
        String[] sortParams = filterOptionsUsers.getSort().get().split(" ");

        switch (sortParams[0]) {
            case "username":
                queryString.append(" username ");
                break;
            case "firstName":
                queryString.append(" firstName ");
                break;
            case "lastName":
                queryString.append(" lastName ");
                break;
            case "rank":
                queryString.append(" points ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }

        if (sortParams.length > 1) {
            if (sortParams[1].equalsIgnoreCase("desc")) {
                queryString.append(" desc ");
            } else if (sortParams[1].equalsIgnoreCase("asc")) {
                queryString.append(" asc ");
            } else {
                throw new UnsupportedOperationException(
                        "The second parameter in the sort should be either 'asc' or 'desc'!");
            }
        }

        return queryString.toString();
    }

    @Override
    public User get(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User get(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    @Override
    public User getByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where resetPasswordToken = :token", User.class);
            query.setParameter("token", token);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "token", token);
            }
            return result.get(0);
        }
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }
}
