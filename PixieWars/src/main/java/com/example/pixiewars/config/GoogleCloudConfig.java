package com.example.pixiewars.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Objects;

@Configuration
public class GoogleCloudConfig {
    @Bean
    public Storage storage() throws IOException {
        GoogleCredentials credentials = GoogleCredentials.
                fromStream(Objects.requireNonNull(getClass().getResourceAsStream("/pixiewars-json7.json")));
        StorageOptions options = StorageOptions.newBuilder()
                .setCredentials(credentials)
                .build();
        return options.getService();
    }
}