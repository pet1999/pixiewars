package com.example.pixiewars.controllers.mvc;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.helpers.AuthenticationHelper;
import com.example.pixiewars.helpers.FileUploadUtil;
import com.example.pixiewars.helpers.UserMapper;
import com.example.pixiewars.models.*;
import com.example.pixiewars.models.dtos.FilterUsersDto;
import com.example.pixiewars.models.dtos.PasswordDto;
import com.example.pixiewars.models.dtos.UserDto;
import com.example.pixiewars.services.contracts.ContestService;
import com.example.pixiewars.services.contracts.InvitationService;
import com.example.pixiewars.services.contracts.ParticipationService;
import com.example.pixiewars.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final ContestService contestService;
    private final ParticipationService participationService;
    private final InvitationService invitationService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final FileUploadUtil fileUploadUtil;

    @Autowired
    public UserMvcController(UserService userService, ContestService contestService, UserMapper userMapper, ParticipationService participationService, InvitationService invitationService, AuthenticationHelper authenticationHelper, FileUploadUtil fileUploadUtil) {
        this.userService = userService;
        this.contestService = contestService;
        this.userMapper = userMapper;
        this.participationService = participationService;
        this.invitationService = invitationService;
        this.authenticationHelper = authenticationHelper;
        this.fileUploadUtil = fileUploadUtil;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("isOrganiser")
    public boolean populateIsOrganiser(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            String userEmail = (String) session.getAttribute("currentUser");
            User user = userService.getByEmail(userEmail);
            return user.isOrganiser();
        }
        return false;
    }

    @ModelAttribute("authUser")
    public User currentUSer(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            String userEmail = (String) session.getAttribute("currentUser");
            return userService.getByEmail(userEmail);
        }
        return null;
    }

    @GetMapping
    public String showAllUsers(@ModelAttribute("filterUsers") FilterUsersDto dto, Model model, HttpSession session) {
        User authenticated;
        try {
            authenticated = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        FilterOptionsUsers fo = new FilterOptionsUsers(dto.getUsername(), dto.getFirstName(), dto.getLastName(),
                dto.getOrganizer(), dto.getRank(), dto.getSort());

        model.addAttribute("filterUsers", dto);
        model.addAttribute("users", userService.filter(authenticated, fo));

        return "UsersView";
    }

    @GetMapping("/{id}")
    public String showSingleUserAndParticipations(@PathVariable int id, Model model, HttpSession session) {
        User authenticated;
        try {
            authenticated = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        User user;
        try {
            user = userService.get(id);
            model.addAttribute("user", user);
            Boolean isCreator = user.getEmail().equals(authenticated.getEmail());
            model.addAttribute("isCreator", isCreator);

            List<Participation> allParticipations = participationService.getAllByUser(id);
            model.addAttribute("allParticipations", allParticipations);

            List<Invitation> allInvitations = invitationService.getAllByUser(authenticated, id);
            model.addAttribute("allInvitations", allInvitations);

            model.addAttribute("juryContests", userService.getAllContestsWhereUserIsJury(authenticated, id));

            return "UserView";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User authenticated;
        try {
            authenticated = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        User user;
        try {
            user = userService.get(id);
            UserDto userDto = userMapper.toDto(user);
            Boolean isCreator = user.getEmail().equals(authenticated.getEmail());
            model.addAttribute("isCreator", isCreator);
            model.addAttribute("user", userDto);
            model.addAttribute("userId", user.getId());
            return "UserUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserDto dto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session,
                             @RequestParam("file") MultipartFile file) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            User user = userService.get(id);
            Boolean isCreator = user.getEmail().equals(authenticatedUser.getEmail());
            model.addAttribute("isCreator", isCreator);
            model.addAttribute("userId", user.getId());
            return "UserUpdateView";
        }

        try {
            User user = userMapper.fromDto(id, dto);
            if (!file.isEmpty()) {
                user.setAvatarPhoto(fileUploadUtil.addPhoto(file));
            }
            userService.update(authenticatedUser, user);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/update/password")
    public String showEditPasswordPage(@PathVariable int id, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        try {
            setChangePasswordModels(id, model, authenticatedUser);
            model.addAttribute("password", new PasswordDto());
            return "ChangePasswordView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update/password")
    public String updatePassword(@PathVariable int id,
                                 @Valid @ModelAttribute("password") PasswordDto dto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session) {
        User authenticatedUser;
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            setChangePasswordModels(id, model, authenticatedUser);
            model.addAttribute("password", new PasswordDto());
            return "ChangePasswordView";
        }

        if (!passwordEncoder.matches(dto.getOldPassword(), authenticatedUser.getPassword())) {
            setChangePasswordModels(id, model, authenticatedUser);
            bindingResult.rejectValue("oldPassword", "password_error", "Entered password should match old password.");
            return "ChangePasswordView";
        }

        if (!dto.getNewPassword().equals(dto.getNewPasswordConfirm())) {
            setChangePasswordModels(id, model, authenticatedUser);
            bindingResult.rejectValue("newPasswordConfirm", "password_error", "Password confirmation should match password.");
            return "ChangePasswordView";
        }

        try {
            User user = userService.get(id);
            userService.updatePassword(user, dto.getNewPassword());
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    private void setChangePasswordModels(int id, Model model, User authenticatedUser) {
        User user = userService.get(id);
        Boolean isCreator = user.getEmail().equals(authenticatedUser.getEmail());
        model.addAttribute("isCreator", isCreator);
        model.addAttribute("userId", user.getId());
    }

    @GetMapping("/{id}/make-organiser")
    public String makeOrganiser(@PathVariable int id, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.makeOrganiser(authenticatedUser, id);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/demote-organiser")
    public String demoteOrganiser(@PathVariable int id, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.demoteOrganiser(authenticatedUser, id);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/invitations/{invitationId}/accept")
    public String acceptInvitation(@PathVariable int id, @PathVariable int invitationId, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            Invitation invitation = invitationService.get(invitationId);
            invitation.setAnswer(true);
            invitationService.answerInvitationJury(authenticatedUser, invitation);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/invitations/{invitationId}/reject")
    public String rejectInvitation(@PathVariable int id, @PathVariable int invitationId, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
        try {
            Invitation invitation = invitationService.get(invitationId);
            invitation.setAnswer(false);
            invitationService.answerInvitationJury(authenticatedUser, invitation);
            return "redirect:/users/" + id + "/invitations";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {

            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, authenticatedUser);
            if (authenticatedUser.getId() == id) {
                session.removeAttribute("currentUser");
                return "redirect:/";
            }
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }
}


