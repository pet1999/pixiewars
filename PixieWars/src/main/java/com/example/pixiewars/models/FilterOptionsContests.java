package com.example.pixiewars.models;

import java.util.Optional;

public class FilterOptionsContests {

    private Optional<String> contestTitle;
    private Optional<Integer> categoryId;
    private Optional<Boolean> isPrivate;
    private Optional<Boolean> isChecked;
    private Optional<String> phase;
    private Optional<String> sort;

    public FilterOptionsContests() {
        this(null, null, null, null, null, null);
    }

    public FilterOptionsContests(String contestTitle, Integer categoryId, Boolean isPrivate,
                                 Boolean isChecked, String phase, String sort) {
        this.contestTitle = Optional.ofNullable(contestTitle);
        this.categoryId = Optional.ofNullable(categoryId);
        this.isPrivate = Optional.ofNullable(isPrivate);
        this.isChecked = Optional.ofNullable(isChecked);
        this.phase = Optional.ofNullable(phase);
        this.sort = Optional.ofNullable(sort);
    }

    public Optional<String> getContestTitle() {
        return contestTitle;
    }

    public Optional<Integer> getCategoryId() {
        return categoryId;
    }

    public Optional<Boolean> getIsPrivate() {
        return isPrivate;
    }

    public Optional<Boolean> getIsChecked() {
        return isChecked;
    }

    public Optional<String> getPhase() {
        return phase;
    }

    public Optional<String> getSort() {
        return sort;
    }
}
