package com.example.pixiewars.services.contracts;

import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    List<User> filter(User authenticatedUser, FilterOptionsUsers filterOptionsUsers);

    User get(int id);

    User get(String username);

    User getByEmail(String email);

    User create(User user);

    void update(User authenticatedUser, User user);

    void delete(int id, User authenticatedUser);

    void makeOrganiser(User authenticatedUser, int userId);

    void demoteOrganiser(User authenticatedUser, int userId);

    void updateResetPasswordToken(String token, String email);

    User getByResetPasswordToken(String token);

    void updatePassword(User user, String newPassword);

    List<Contest> getAllContestsWhereUserIsJury(User authenticatedUser, int userId);


}
