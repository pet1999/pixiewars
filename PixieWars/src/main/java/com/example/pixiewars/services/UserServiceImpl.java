package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.exceptions.EntityDuplicateException;
import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.models.FilterOptionsUsers;
import com.example.pixiewars.models.User;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import com.example.pixiewars.services.contracts.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;

@Service
public class UserServiceImpl implements UserService {

    public static final String YOU_CANNOT_CHANGE_VARIABLE = "You cannot change %s.";
    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";

    private final UserRepository repository;

    private final ContestRepository contestRepository;
    private final ParticipationRepository participationRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, ContestRepository contestRepository, ParticipationRepository participationRepository) {
        this.repository = repository;
        this.contestRepository = contestRepository;
        this.participationRepository = participationRepository;
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public List<User> filter(User authenticatedUser, FilterOptionsUsers filterOptionsUsers) {
        return repository.filter(filterOptionsUsers);
    }

    @Override
    public User get(int id) {
        return repository.get(id);
    }

    @Override
    public User get(String username) {
        return repository.get(username);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public User create(User user) {
        boolean duplicateUsernameExists = true;
        boolean duplicateEmailExists = true;
        try {
            repository.get(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }
        try {
            repository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }
        if (duplicateUsernameExists) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }
        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        user.setRank("Junkie");
        encodePassword(user);
        return repository.create(user);
    }

    @Override
    public void update(User authenticatedUser, User user) {
        User existingUser = repository.get(user.getId());
        checkModifyPermissions(existingUser, authenticatedUser);
        if (!existingUser.getUsername().equalsIgnoreCase(user.getUsername())) {
            throw new AuthorizationException(format(YOU_CANNOT_CHANGE_VARIABLE, "username"));
        }
        if (!existingUser.getEmail().equalsIgnoreCase(user.getEmail())) {
            throw new AuthorizationException(format(YOU_CANNOT_CHANGE_VARIABLE, "email"));
        }
        repository.update(user);
    }

    @Override
    public void delete(int id, User authenticatedUser) {
        User user = repository.get(id);
        checkModifyPermissions(user, authenticatedUser);
        user.setUsername(user.getUsername() + "(deleted)");
        user.setEmail(user.getEmail() + "(deleted)");
        user.setDeleted(true);
        repository.update(user);
    }

    @Override
    public void makeOrganiser(User authenticatedUser, int userId) {
        checkIsAdmin(authenticatedUser);
        User existingUser = repository.get(userId);
        if (!existingUser.isOrganiser()) {
            existingUser.setOrganiser(true);
            repository.update(existingUser);
        }
    }

    @Override
    public void demoteOrganiser(User authenticatedUser, int userId) {
        checkIsAdmin(authenticatedUser);
        User existingUser = repository.get(userId);
        if (existingUser.isOrganiser() && !existingUser.equals(authenticatedUser)) {
            existingUser.setOrganiser(false);
            repository.update(existingUser);
        } else {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

    @Override
    public List<Contest> getAllContestsWhereUserIsJury(User authenticatedUser, int userId) {
        User userToGetContests = repository.get(userId);
        List<Contest> contestsToReturn = new ArrayList<>();
        List<Contest> allContestsInPhaseTwo = contestRepository.filter(new FilterOptionsContests(null, null, null, null,
                "phase_two", null));
        for (Contest contest : allContestsInPhaseTwo) {
            Set<User> juryMembers = contest.getJuryMembers();
            if (juryMembers.contains(userToGetContests)) {
                contestsToReturn.add(contest);
            }
        }
        return contestsToReturn;
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = repository.getByEmail(email);
        user.setResetPasswordToken(token);
        LocalDateTime currentDate = LocalDateTime.now();
        Date date = Date.from(currentDate.atZone(ZoneId.systemDefault()).toInstant());
        Date expiryDate = DateUtils.addHours(date, 1);
        user.setTokenExpiryDate(expiryDate);
        repository.update(user);
    }

    @Override
    public User getByResetPasswordToken(String token) {
        LocalDateTime currentDate = LocalDateTime.now();
        User user = repository.getByToken(token);
        LocalDateTime expiryDate = repository.getByToken(token).getTokenExpiryDate().
                toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        if(currentDate.isAfter(expiryDate)){
            user.setTokenExpiryDate(null);
            user.setResetPasswordToken(null);
            repository.update(user);
        }
        return repository.getByToken(token);
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        encodePassword(user);
        user.setResetPasswordToken(null);
        repository.update(user);
    }


    private void checkModifyPermissions(User user, User authenticatedUser) {
        if (!authenticatedUser.isOrganiser()) {
            if (!user.getUsername().equalsIgnoreCase(authenticatedUser.getUsername())) {
                throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
            }
        }
    }

    private void checkIsAdmin(User authenticatedUser) {
        if (!authenticatedUser.isOrganiser()) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

    private void encodePassword(User user) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
    }
}
