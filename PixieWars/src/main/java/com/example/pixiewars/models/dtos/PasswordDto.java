package com.example.pixiewars.models.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class PasswordDto {
    @NotEmpty(message = "Old password can't be empty")
    private String oldPassword;
    @Size(min = 7, max = 20, message = "Password should be between 7 and 20 symbols.")
    private String newPassword;
    @NotEmpty(message = "Password confirmation can't be empty")
    private String newPasswordConfirm;

    public PasswordDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirm() {
        return newPasswordConfirm;
    }

    public void setNewPasswordConfirm(String newPasswordConfirm) {
        this.newPasswordConfirm = newPasswordConfirm;
    }
}
