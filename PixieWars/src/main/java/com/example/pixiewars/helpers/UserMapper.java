package com.example.pixiewars.helpers;

import com.example.pixiewars.models.RegisterDto;
import com.example.pixiewars.models.User;
import com.example.pixiewars.models.dtos.UserDto;
import com.example.pixiewars.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static final String PREVIOUS_PICTURE = "previous picture";
    private final UserService userService;


    @Autowired
    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User fromDto(int id, UserDto dto) {
        User user = fromDto(dto);

        user.setId(id);
        User repositoryUser = userService.get(id);
        user.setPoints(repositoryUser.getPoints());
        user.setRank(repositoryUser.getRank());
        user.setOrganiser(repositoryUser.isOrganiser());
        user.setAvatarPhoto(repositoryUser.getAvatarPhoto());
        return user;
    }

    public User fromDto(UserDto dto) { // REST
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        return user;
    }

    public User fromDto(RegisterDto registerDto) { // MVC
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPassword(user.getPassword());
        userDto.setEmail(user.getEmail());
        userDto.setAvatar_url(user.getAvatarPhoto());
        return userDto;
    }
}
