package com.example.pixiewars.repositories;

import com.example.pixiewars.exceptions.EntityNotFoundException;
import com.example.pixiewars.models.Contest;
import com.example.pixiewars.models.FilterOptionsContests;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContestRepositoryImpl implements ContestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest", Contest.class);
            return query.list();
        }
    }

    @Override
    public List<Contest> filter(FilterOptionsContests filterOptionsContests) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Contest> query = builder.createQuery(Contest.class);
            Root<Contest> root = query.from(Contest.class);

            StringBuilder queryString = new StringBuilder(" from Contest ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filterOptionsContests.getContestTitle().ifPresent(v -> {
                filter.add(" title like :title ");
                queryParams.put("title", "%" + v + "%");
            });
            filterOptionsContests.getCategoryId().ifPresent(value -> {
                filter.add("category.id = :categoryId");
                queryParams.put("categoryId", value);
            });
            filterOptionsContests.getIsPrivate().ifPresent(value -> {
                filter.add("isPrivate = :isPrivate");
                queryParams.put("isPrivate", value);
            });
            filterOptionsContests.getIsChecked().ifPresent(value -> {
                filter.add("isChecked = :isChecked");
                queryParams.put("isChecked", value);
            });

            Date currentDate = new Date();
            filterOptionsContests.getPhase().ifPresent(phase -> {
                switch (phase) {
                    case "phase_one":
                        filter.add(":currentDate <= phase_one_time_limit");
                        queryParams.put("currentDate", currentDate);
                        break;
                    case "phase_two":
                        filter.add(":currentDate > phase_one_time_limit AND :currentDate <= phase_two_time_limit");
                        queryParams.put("currentDate", currentDate);
                        break;
                    case "finished":
                        filter.add(":currentDate > phase_two_time_limit");
                        queryParams.put("currentDate", currentDate);
                        break;
                }
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }
            queryString.append(generateSortBy(filterOptionsContests));
            Query<Contest> queryList = session.createQuery(queryString.toString(), Contest.class);
            queryList.setProperties(queryParams);
            System.out.println(queryString);
            return queryList.list();
        }
    }

    private String generateSortBy(FilterOptionsContests filterOptionsContests) {
        StringBuilder queryString = new StringBuilder(" order by ");

        if (filterOptionsContests.getSort().isEmpty()) {
            return "";
        }
        String[] sortParams = filterOptionsContests.getSort().get().split(" ");

        switch (sortParams[0]) {
            case "title":
                queryString.append(" title ");
                break;
            case "prize":
                queryString.append(" prize ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }

        if (sortParams.length > 1) {
            if (sortParams[1].equalsIgnoreCase("desc")) {
                queryString.append(" desc ");
            } else if (sortParams[1].equalsIgnoreCase("asc")) {
                queryString.append(" asc ");
            } else {
                throw new UnsupportedOperationException(
                        "The second parameter in the sort should be either 'asc' or 'desc'!");
            }
        }

        return queryString.toString();
    }

    @Override
    public Contest get(int id) {
        try (Session session = sessionFactory.openSession()) {
            Contest contest = session.get(Contest.class, id);
            if (contest == null) {
                throw new EntityNotFoundException("Contest", id);
            }
            return contest;
        }
    }

    @Override
    public Contest get(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where title = :title", Contest.class);
            query.setParameter("title", title);

            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Contest", "title", title);
            }

            return result.get(0);
        }
    }

    @Override
    public Contest create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(contest);
            session.getTransaction().commit();
            return contest;
        }
    }

    @Override
    public Contest update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(contest);
            session.getTransaction().commit();
            return contest;
        }
    }

    @Override
    public void delete(int contestId) {
        Contest contestToDelete = get(contestId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(contestToDelete);
            session.getTransaction().commit();
        }
    }
}
