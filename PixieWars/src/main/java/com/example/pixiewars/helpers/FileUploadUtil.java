package com.example.pixiewars.helpers;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.example.pixiewars.config.GoogleCloudConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.io.IOException;
import java.nio.file.Paths;

import java.io.IOException;
import java.util.Objects;

@Component
public class FileUploadUtil {
    private final GoogleCloudConfig googleCloudConfig;

    @Autowired
    public FileUploadUtil(GoogleCloudConfig googleCloudConfig) {
        this.googleCloudConfig = googleCloudConfig;
    }

    public void saveFile(String bucketName, String objectKey, MultipartFile file) throws IOException {
        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, objectKey)
                .setContentType(file.getContentType())
                .build();
        googleCloudConfig.storage().create(blobInfo, file.getInputStream());
    }

    public String addPhoto(MultipartFile file) {
        try {
            String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
            String bucketName = "pixiewars7";
            String photoPath = "https://storage.googleapis.com/" + bucketName + "/" + filename;
            saveFile(bucketName, filename, file);
            return photoPath;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
