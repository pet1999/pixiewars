package com.example.pixiewars.services;

import com.example.pixiewars.exceptions.AuthorizationException;
import com.example.pixiewars.models.*;
import com.example.pixiewars.repositories.contracts.ContestRepository;
import com.example.pixiewars.repositories.contracts.InvitationRepository;
import com.example.pixiewars.repositories.contracts.ParticipationRepository;
import com.example.pixiewars.repositories.contracts.UserRepository;
import com.example.pixiewars.services.contracts.InvitationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class InvitationServiceImpl implements InvitationService {

    public static final String JURY_MESSAGE = "Hi %s, We invite you to join as Jury to %s.%nYou can accept this invitation until %s";
    public static final String PARTICIPATION_MESSAGE = "Hi %s, We invite you to %s.%nYou can join until %s";

    private static final String MODIFY_USER_ERROR_MESSAGE = "You are not authorised.";

    private static final String PARTICIPATION_INVITE_ERROR_MESSAGE = "You are jury, you can't participate.";
    private static final String JURY_INVITE_ERROR_MESSAGE = "You are participant, you can't be jury member.";



    private final InvitationRepository repository;
    private final UserRepository userRepository;
    private final ContestRepository contestRepository;
    private final ParticipationRepository participationRepository;


    @Autowired
    public InvitationServiceImpl(InvitationRepository repository, UserRepository userRepository, ContestRepository contestRepository, ParticipationRepository participationRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.contestRepository = contestRepository;
        this.participationRepository = participationRepository;
    }

    @Override
    public List<Invitation> getAllByContest(int contestId, FilterOptions filterOptions) {
        contestRepository.get(contestId);
        return repository.getAllByContest(contestId, filterOptions);
    }

    @Override
    public List<Invitation> getAllByUser(User authenticated, int userId) {
        User currentUser = userRepository.get(userId);
        return repository.getAllByUser(userId);

    }

    @Override
    public Invitation get(int invitationId) {
        return repository.get(invitationId);
    }

    @Override
    public Invitation inviteJury(int userId, int contestId) {
        User user = userRepository.get(userId);
        Contest contest = contestRepository.get(contestId);
        List<Participation> contestParticipationsByUser = participationRepository.getAllByContest(contestId,new FilterOptions())
                .stream().filter(participation -> participation.getCreator().equals(user)).toList();
        if(contestParticipationsByUser.size() > 0){
            throw new AuthorizationException(JURY_INVITE_ERROR_MESSAGE);
        }
        Invitation invitation = new Invitation();
        invitation.setContestId(contestId);
        invitation.setUserId(userId);
        invitation.setForJury(true);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
        invitation.setMessage(String.format
                (JURY_MESSAGE, user.getFirstName(), contest.getTitle(), formatter.format(contest.getPhase_one_time_limit())));
        return repository.create(invitation);
    }

    @Override
    public Invitation inviteParticipation(int userId, int contestId) {
        User user = userRepository.get(userId);
        if (user.isOrganiser()) {
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
        Contest contest = contestRepository.get(contestId);
        if(contest.getJuryMembers().contains(user)){
            throw new AuthorizationException(PARTICIPATION_INVITE_ERROR_MESSAGE);
        }
        Invitation invitation = new Invitation();
        invitation.setContestId(contestId);
        invitation.setUserId(userId);
        invitation.setForJury(false);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
        invitation.setMessage(String.format
                (PARTICIPATION_MESSAGE, user.getFirstName(), contest.getTitle(),formatter.format(contest.getPhase_one_time_limit())));

        return repository.create(invitation);
    }

    @Override
    public void answerInvitationJury(User authtenticatedUser, Invitation invitation) {
        Contest contest = contestRepository.get(invitation.getContestId());
        if(!invitation.getForJury()){
            throw new AuthorizationException(MODIFY_USER_ERROR_MESSAGE);
        }
        if (invitation.getAnswer()) {
            contest.getJuryMembers().add(authtenticatedUser);
            contestRepository.update(contest);
        }
        delete(invitation.getInvitationId());
    }

    @Override
    public void delete(int invitationId) {
        Invitation invitation = repository.get(invitationId);
        List<Invitation> userInvitations = repository.getAllByContest(invitation.getContestId(),new FilterOptions())
                .stream().filter(invitation1 -> invitation1.getUserId() == invitation.getUserId()).toList();
        for (Invitation userInvitation : userInvitations) {
            repository.delete(userInvitation.getInvitationId());
        }
    }
}
