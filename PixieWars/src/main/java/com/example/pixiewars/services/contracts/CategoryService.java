package com.example.pixiewars.services.contracts;

import com.example.pixiewars.models.Category;
import com.example.pixiewars.models.User;

import java.util.List;

public interface CategoryService {

    List<Category> get();

    Category get(int id);

    Category getByName(String categoryName);

    Category create(User authenticatedUser, Category category);

}
