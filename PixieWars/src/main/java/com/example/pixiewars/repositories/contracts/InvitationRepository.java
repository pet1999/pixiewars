package com.example.pixiewars.repositories.contracts;

import com.example.pixiewars.models.FilterOptions;
import com.example.pixiewars.models.Invitation;

import java.util.List;

public interface InvitationRepository {
    List<Invitation> getAllByContest(int contestId, FilterOptions filterOptions);

    List<Invitation> getAllByUser(int userId);

    Invitation get(int invitationId);

    Invitation create(Invitation invitation);

    void delete(int invitationId);
}
